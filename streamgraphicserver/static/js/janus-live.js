var janusServer = null;
if (window.location.protocol === 'http:')
	janusServer = "http://" + window.location.hostname + ":8088/janus";
else
	janusServer = "https://" + window.location.hostname + ":8089/janus";

// Override the WebRTC support check to ensure this works in non-secure contexts.
Janus.isWebrtcSupported = function () {
	return window.RTCPeerConnection !== undefined && window.RTCPeerConnection !== null;
};

var janusObjects = {};
var janusStreamingObjects = {};

function watchJanusStream(selectedStream, videoElementID) {
	var opaqueId = "streamingtest-" + Janus.randomString(12);
	janusObjects[videoElementID] = null;
	janusStreamingObjects[videoElementID] = null;

	// Initialize the library (all console debuggers enabled)
	Janus.init({
		debug: "all",
		callback: function () {
			// Make sure the browser supports WebRTC
			if (!Janus.isWebrtcSupported()) {
				console.log("No WebRTC support... ");
				return;
			}
			// Create session
			janusObjects[videoElementID] = new Janus({
				server: janusServer,
				success: function () {
					// Attach to Streaming plugin
					janusObjects[videoElementID].attach({
						plugin: "janus.plugin.streaming",
						opaqueId: opaqueId,
						success: function (pluginHandle) {
							janusStreamingObjects[videoElementID] = pluginHandle;
							Janus.log("Plugin attached! (" + janusStreamingObjects[videoElementID].getPlugin() + ", id=" + janusStreamingObjects[videoElementID].getId() + ")");
							// Setup streaming session
							var body = {
								request: "list"
							};
							Janus.debug("Sending message:", body);
							janusStreamingObjects[videoElementID].send({
								message: body,
								success: function (result) {
									if (!result) {
										console.log("Got no response to our query for available streams");
										return;
									}
									if (result["list"]) {
										var list = result["list"];
										Janus.log("Got a list of available streams");
										if (list && Array.isArray(list)) {
											list.sort(function (a, b) {
												if (!a || a.id < (b ? b.id : 0))
													return -1;
												if (!b || b.id < (a ? a.id : 0))
													return 1;
												return 0;
											});
										}
										Janus.debug(list);
										for (var mp in list) {
											console.log("  >> [" + list[mp]["id"] + "] " + list[mp]["description"] + " (" + list[mp]["type"] + ")");
										}
									}
								}
							});

							Janus.log("Selected video id #" + selectedStream);
							if (!selectedStream) {
								console.log("Select a stream from the list");
								return;
							}
							body = {
								request: "watch",
								id: parseInt(selectedStream) || selectedStream
							};
							janusStreamingObjects[videoElementID].send({
								message: body
							});
							// Get some more info for the mountpoint to display, if any
							if (!selectedStream)
								return;
							// Send a request for more info on the mountpoint we subscribed to
							body = {
								request: "info",
								id: parseInt(selectedStream) || selectedStream
							};
							janusStreamingObjects[videoElementID].send({
								message: body,
								success: function (result) {
									if (result && result.info && result.info.metadata) {
										console.log(result.info.metadata);
									}
								}
							});
						},
						error: function (error) {
							Janus.error("  -- Error attaching plugin... ", error);
						},
						iceState: function (state) {
							Janus.log("ICE state changed to " + state);
						},
						webrtcState: function (on) {
							Janus.log("Janus says our WebRTC PeerConnection is " + (on ? "up" : "down") + " now");
						},
						onmessage: function (msg, jsep) {
							Janus.debug(" ::: Got a message :::", msg);
							var result = msg["result"];
							if (result) {
								if (result["status"]) {
									var status = result["status"];
									if (status === 'starting')
										console.log("Starting, please wait...");
									else if (status === 'started')
										console.log("Started");
									else if (status === 'stopped') {
										endJanusStream(videoElementID);
									}
								} else if (msg["streaming"] === "event") {
									console.log("WARNING: Simulcast triggered");
								}
							} else if (msg["error"]) {
								console.log(msg["error"]);
								endJanusStream(videoElementID);
								return;
							}
							if (jsep) {
								Janus.debug("Handling SDP as well...", jsep);
								var stereo = (jsep.sdp.indexOf("stereo=1") !== -1);
								// Offer from the plugin, let's answer
								janusStreamingObjects[videoElementID].createAnswer({
									jsep: jsep,
									// We want recvonly audio/video and, if negotiated, datachannels
									media: {
										audioSend: false,
										videoSend: false,
										data: true
									},
									customizeSdp: function (jsep) {
										if (stereo && jsep.sdp.indexOf("stereo=1") == -1) {
											// Make sure that our offer contains stereo too
											jsep.sdp = jsep.sdp.replace("useinbandfec=1", "useinbandfec=1;stereo=1");
										}
									},
									success: function (jsep) {
										Janus.debug("Got SDP!", jsep);
										var body = {
											request: "start"
										};
										janusStreamingObjects[videoElementID].send({
											message: body,
											jsep: jsep
										});
									},
									error: function (error) {
										Janus.error("WebRTC error:", error);
									}
								});
							}
						},
						onremotestream: function (stream) {
							Janus.debug(" ::: Got a remote stream :::", stream);
							var videoTracks = stream.getVideoTracks();
							if (!videoTracks || videoTracks.length === 0)
								return;
							var remote_video_elem = document.getElementById(videoElementID);
							Janus.attachMediaStream(remote_video_elem, stream);
							remote_video_elem.play();
						}
					});
				},
				error: function (error) {
					Janus.error(error);
				},
				destroyed: function () {
					// window.location.reload();
				}
			});

		}
	});
}

function endJanusStream(videoElementID) {
	console.log('Shutting down janus stream for ' + videoElementID);
	var body = {
		request: "stop"
	};
	janusStreamingObjects[videoElementID].send({
		message: body
	});
	janusStreamingObjects[videoElementID].hangup();
	console.log('Shut down janus stream for ' + videoElementID);
}
