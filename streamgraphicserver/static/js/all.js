SVGInject.setOptions({
    makeIdsUnique: false,
    afterInject: function (img, svg) {
        svg.style.width = "auto";
        svg.style.height = "auto";
    }
});

const url_params = new URLSearchParams(window.location.search);
const default_lang = 'en';

if (fallback) {
    var socket = io("/websocket", {
        jsonp: false
    });

    socket.on('connect', () => {
        console.log(socket.id);
    });

    socket.on('connect', function () {
        console.info('connect');
    });
    socket.on('connect_error', function (error) {
        console.info('connect_error');
    });
    socket.on('disconnect', function (reason) {
        console.info('disconnect');
    });

    socket.io.on('reconnect', function (attemptNumber) {
        console.info('reconnect');
    });
    socket.io.on('reconnect_error', function (error) {
        console.info('reconnect_error');
    });
    socket.io.on('reconnect_failed', function () {
        console.info('reconnect_failed');
    });
    socket.io.on('error', function (error) {
        console.info('error');
    });

    socket.on('update', function (msg) {
        handle_message({
            data: {
                type: 'update',
                msg: msg
            }
        });
    });
    socket.on('update_multiple', function (msg) {
        handle_message({
            data: {
                type: 'update_multiple',
                msg: msg
            }
        });
    });
    socket.on('update_compressed', function (msg) {
        handle_message({
            data: {
                type: 'update_compressed',
                msg: msg
            }
        });
    });
    socket.on('update_multiple_compressed', function (msg) {
        handle_message({
            data: {
                type: 'update_multiple_compressed',
                msg: msg
            }
        });
    });
    socket.on('alert_overlay', function (msg) {
        handle_message({
            data: {
                type: 'alert_overlay',
                msg: msg
            }
        });
    });

} else {
    if (!!window.SharedWorker) {
        window.addEventListener('load', function () {
            var socketio_worker = new SharedWorker('/static/js/worker.js');

            socketio_worker.port.start();
        });
    }

    var channel = new BroadcastChannel('sw-socketio-msgs');

    channel.addEventListener('message', event => {
        handle_message(event);
    });

    setInterval(function () {
        channel.postMessage('ping');
    }, 20000);
}

function decompress_json(msg) {
    var str_data = atob(msg);
    var char_data = str_data.split('').map(function (x) {
        return x.charCodeAt(0);
    });
    var bin_data = new Uint8Array(char_data);
    var decompressed_data = pako.inflate(bin_data);

    var json_data_str = new TextDecoder("utf-8").decode(decompressed_data);
    return JSON.parse(json_data_str);
}

function handle_message(event) {
    var msg_type = event.data.type;
    var msg = event.data.msg;

    if (msg_type === 'update') {
        let path = msg.path;
        let value = msg.data;

        if (data.get(path) !== value) {
            data.set(path, value);
            on_update(path, value);
        }
    } else if (msg_type === 'update_multiple') {
        var changed_values = {};

        for (let [path, value] of Object.entries(msg)) {
            if (data.get(path) !== value) {
                data.set(path, value);
                changed_values[path] = value;
            }
        }

        for (let [path, value] of Object.entries(changed_values)) {
            on_update(path, value);
        }
    } else if (msg_type === 'update_compressed') {
        msg = decompress_json(msg);

        let path = msg.path;
        let value = msg.data;

        if (data.get(path) !== value) {
            data.set(path, value);
            on_update(path, value);
        }
    } else if (msg_type === 'update_multiple_compressed') {
        msg = decompress_json(msg);

        let changed_values = {};

        for (let [path, value] of Object.entries(msg)) {
            if (data.get(path) !== value) {
                data.set(path, value);
                changed_values[path] = value;
            }
        }

        for (let [path, value] of Object.entries(changed_values)) {
            on_update(path, value);
        }
    }
}

// https://stackoverflow.com/a/42156958
function flatten(obj, prefix, current) {
    prefix = prefix || [];
    current = current || {};

    // Remember kids, null is also an object!
    if (typeof (obj) === 'object' && obj !== null) {
        Object.keys(obj).forEach(key => {
            this.flatten(obj[key], prefix.concat(key), current);
        });
    } else {
        current[prefix.join('/')] = obj;
    }

    return current;
}

function unflatten(object) {
    var result = {};
    Object.keys(object).forEach(function (k) {
        setValue(result, k, object[k]);
    });
    return result;
}

// function get_flattened_styles_from_key(search) {
//     let filtered_data = Object.entries(data).filter(([k, v]) => k.startsWith(search + '/'));
//     let renamed_data_keys = filtered_data.map(([k, v]) => [k.replace(search + '/', ""), v]);

//     let filtered_data_json = Object.fromEntries(renamed_data_keys);

//     return filtered_data_json;
// }

function filtered_data(search) {
    let filtered_data_json = {};
    data.forEach((value, key) => {
        if (key.startsWith(search)) {
            filtered_data_json[key.replace(search + '/', '')] = value;
        }
    });

    return filtered_data_json;
}

function setValue(object, path, value) {
    var way = path.split('/'),
        last = way.pop();

    way.reduce(function (o, k, i, kk) {
        /* jshint -W093 */
        return o[k] = o[k] || (isFinite(i + 1 in kk ? kk[i + 1] : last) ? [] : {});
    }, object)[last] = value;
}

function varToBoolean(variable, fallback) {
    if (typeof variable === "boolean") {
        return variable;
    } else if (typeof variable === "string") {
        switch (variable.toLowerCase().trim()) {
            case "True":
            case "true":
            case "yes":
            case "1":
                return true;
            case "False":
            case "false":
            case "no":
            case "0":
                return false;
            default:
                return Boolean(fallback);
        }
    } else {
        return Boolean(variable);
    }
}

var data = new Map();

var div_height = 0;
var lol_champ_ver = "11.22.1";
var lol_champ_data = null;
var lol_champ_data_corrected = {};
var lol_champ_data_id = {};
var lol_summoner_spell_id = {};
var lol_perk_id = {};

var all_styles = [
    'bg',
    'border-top-left-radius',
    'border-top-right-radius',
    'border-bottom-left-radius',
    'border-bottom-right-radius',
    'border-style-top',
    'border-style-bottom',
    'border-style-left',
    'border-style-right',
    'display',
    'image-correction',
    'pos-h',
    'pos-w',
    'pos-x',
    'pos-y',
    'skew',
    'text-align',
    'text-color',
    'text-font',
    'text-size',
    'text-style',
    'white-space'
];

function get_all_data(callback) {
    var request = new XMLHttpRequest();
    request.open('GET', '/api/v1/export', true);

    request.onload = function () {
        if (this.status >= 200 && this.status < 400) {
            data = new Map(Object.entries(JSON.parse(this.response)));
            callback();
        } else {
            console.log("Error downloading init data.");

        }
    };

    request.send();
}

function get_all_component_styles(component_name) {
    // Old: Highly inefficient.
    // return Object.keys(data).filter(propertyName => propertyName.startsWith('component/' + component_name + "/style/"));

    filtered_styles = [];

    all_styles.forEach(function (style) {
        key_name_lookup = 'component/' + component_name + "/style/" + style;
        if (data.has(key_name_lookup)) {
            filtered_styles.push(key_name_lookup);
        }
    });

    return filtered_styles;
}

function on_update(path, value) {

    //console.log('Update:', path, value);

    var split_path = path.split('/');


    if (split_path[0] === 'data') {
        if (typeof display_data_update === "function") {
            display_data_update(path);
        }
        if (typeof form_data_update === "function") {
            form_data_update(path);
        }
    } else if (split_path[0] === 'overlay') {
        if (document.getElementById('overlay/' + split_path[1])) {
            if (typeof display_overlay_update === "function") {
                display_overlay_update(path);
            }
            if (typeof form_overlay_update === "function") {
                form_overlay_update(path);
            }
        }
    } else if (split_path[0] === 'layer') {
        if (document.getElementById('layer/' + split_path[1])) {
            if (typeof display_layer_update === "function") {
                display_layer_update(path);
            }
            if (typeof form_layer_update === "function") {
                form_layer_update(path);
            }
        }
    } else if (split_path[0] === 'component') {
        if (document.getElementById('component/' + split_path[1])) {
            if (typeof display_component_update === "function") {
                display_component_update(path);
            }
            if (typeof form_component_update === "function") {
                form_component_update(path);
            }
        }
    }
}

function getJSON(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function () {
        var status = xhr.status;
        if (status === 200) {
            callback(null, xhr.response);
        } else {
            callback(status, xhr.response);
        }
    };
    xhr.send();
}

getJSON('https://ddragon.leagueoflegends.com/realms/na.json',
    function (err, version) {
        if (err !== null) {
            console.error('Unable to get LoL version info.');
        } else {
            lol_champ_ver = version.n.champion;
            
            getJSON('https://ddragon.leagueoflegends.com/cdn/' + lol_champ_ver + '/data/en_US/champion.json',
                function (err2, champ_data) {
                    if (err2 !== null) {
                        alert('Something went wrong: ' + err2);
                    } else {
                        lol_champ_data = champ_data.data;

                        Object.values(champ_data.data).forEach(champion_info => {
                            lol_champ_data_corrected[champion_info.name] = champion_info;
                            lol_champ_data_id[champion_info.key] = champion_info;
                        });
                    }
                }
            );

            getJSON('https://ddragon.leagueoflegends.com/cdn/' + lol_champ_ver + '/data/en_US/summoner.json',
                function (err2, summoner_spells) {
                    if (err2 !== null) {
                        alert('Something went wrong: ' + err2);
                    } else {
                        Object.values(summoner_spells.data).forEach(summoner_spell => {
                            lol_summoner_spell_id[summoner_spell.key] = summoner_spell;
                        });
                    }
                }
            );

            getJSON('https://ddragon.leagueoflegends.com/cdn/' + lol_champ_ver + '/data/en_US/runesReforged.json',
                function (err2, runes) {
                    if (err2 !== null) {
                        alert('Something went wrong: ' + err2);
                    } else {
                        Object.values(runes).forEach(rune_data => {
                            lol_perk_id[rune_data.id] = rune_data;

                            Object.values(rune_data.slots).forEach(rune_slot => {
                                Object.values(rune_slot.runes).forEach(rune_slot_rune => {
                                    lol_perk_id[rune_slot_rune.id] = rune_slot_rune;
                                });
                            });
                        });
                    }
                }
            );
        }
    }
);