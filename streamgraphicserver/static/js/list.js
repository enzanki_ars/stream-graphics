function create_overlay_list() {
    var request = new XMLHttpRequest();
    request.open('GET', '/api/v1/overlay_list', true);

    request.onload = function () {
        if (this.status >= 200 && this.status < 400) {
            var list_data = JSON.parse(this.response);

            if (window.console) console.log(list_data);

            list_data.sort().forEach(function (value) {
                document.getElementById('overlay-list').innerHTML +=
                    '<div class="col gx-2"><div class="card">' +
                    '    <div class="card-body">\n' +
                    '        <h5 class="card-title">' +
                    '            <a href="/overlay/' + value + '">' + value + '</a>' +
                    '        </h5>\n' +
                    '    </div>\n' +
                    '</div></div>';
            });
        } else {
            if (window.console) console.log("Error downloading init data.");
        }
    };

    request.send();
}

function create_mix_list() {
    var request = new XMLHttpRequest();
    request.open('GET', '/api/v1/mix_list', true);

    request.onload = function () {
        if (this.status >= 200 && this.status < 400) {
            var list_data = JSON.parse(this.response);

            if (window.console) console.log(list_data);

            list_data.sort().forEach(function (value) {
                document.getElementById('mix-list').innerHTML +=
                    '<div class="col gx-2"><div class="card">' +
                    '    <div class="card-body">\n' +
                    '        <h5 class="card-title">' +
                    '            <a href="/mix/' + value + '">' + value + '</a>' +
                    '        </h5>\n' +
                    '    </div>\n' +
                    '</div></div>';
            });
        } else {
            if (window.console) console.log("Error downloading init data.");
        }
    };

    request.send();
}
