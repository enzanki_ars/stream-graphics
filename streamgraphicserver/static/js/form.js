function create_edit_form(path) {
    var request = new XMLHttpRequest();
    request.open('GET', '/api/v1/search_prefix/' + path, true);

    request.onload = function () {
        if (this.status >= 200 && this.status < 400) {
            data = JSON.parse(this.response);

            var table_body = document.getElementById('edit-table-body');

            Object.keys(data).sort().forEach(function (value) {

                var row = table_body.insertRow(-1);

                var key_cell = row.insertCell(0);
                var value_cell = row.insertCell(1);
                var submit_cell = row.insertCell(2);

                var key_label = document.createElement('label');
                key_label.setAttribute('for', 'data-' + value + '-input');
                key_label.innerText = value;

                key_cell.appendChild(key_label);

                var value_input = document.createElement('input');
                value_input.setAttribute('class', 'form-control');
                value_input.classList.add('form-control');
                value_input.type = 'text';
                value_input.id = 'data-' + value + '-input';
                value_input.value = data.get(value);

                value_cell.appendChild(value_input);

                var submit_button = document.createElement('button');
                submit_button.type = 'button';
                submit_button.classList.add('btn');
                submit_button.classList.add('btn-primary');
                submit_button.innerText = 'Send';
                submit_button.onclick = function () {
                    data_send(value);
                };

                submit_cell.appendChild(submit_button);
            });

            if (window.console) console.log(data);
        } else {
            if (window.console) console.log("Error downloading init data.");

        }
    };

    request.send();
}

function data_send(path) {
    var value = document.getElementById('data-' + path + '-input').value;
    if (window.console) console.log('Submitting data for', path, "-", value);

    var data_to_send = {};
    data_to_send[path] = value;

    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/api/v1/set_multiple", true);
    xhttp.send(JSON.stringify(data_to_send));
}

function data_form_get(path) {
    return document.getElementById('data-' + path + '-input').value;
}

function data_form_change(path, value) {
    document.getElementById('data-' + path + '-input').value = value;
}

function raw_send(path, value) {
    if (window.console) console.log('Submitting data for', path, "-", value);

    var data_to_send = {};
    data_to_send[path] = value;
    data_form_change(path, value);

    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/api/v1/set_multiple", true);
    xhttp.send(JSON.stringify(data_to_send));
}

function restore_default(path) {
    if (window.console) console.log('Restoring', path);

    var restore_params = {};

    for (let parameter of document.querySelectorAll("[data-restorename='" + path + "']")) {
        console.log(parameter.dataset.restorevar, parameter.value);
        restore_params[parameter.dataset.restorevar] = parameter.value;
    }

    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/api/v1/restore_default/" + path + formatParams(restore_params), true);
    xhttp.send();
}

function restore(path, value) {
    if (window.console) console.log('Restoring', path, "-", value);

    var restore_params = {};

    for (let parameter of document.querySelectorAll("[data-restorename='" + path + "']")) {
        console.log(parameter.dataset.restorevar, parameter.value);
        restore_params[parameter.dataset.restorevar] = parameter.value;
    }

    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/api/v1/restore/" + path + "/" + value + formatParams(restore_params), true);
    xhttp.send();
}

function reload_restore_properties() {
    if (window.console) console.log('Reloading Restore List');

    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/api/v1/reload_restore_properties", true);
    xhttp.send();
}

function run_script(path) {
    if (window.console) console.log('Running', path);

    var data_to_send = {};

    for (let parameter of document.querySelectorAll("[data-script='" + path + "']")) {
        console.log(parameter.dataset.parameter, parameter.value);
        data_to_send[parameter.dataset.parameter] = parameter.value;
    }

    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "/api/v1/run_data_source/" + path, true);
    xhttp.send(JSON.stringify(data_to_send));
}

function stop_script(path) {
    if (window.console) console.log('Stopping', path);

    var xhttp = new XMLHttpRequest();
    xhttp.open("DELETE", "/api/v1/run_data_source/" + path, true);
    xhttp.send();
}

function form_data_update(path) {

    element = document.getElementById('data-' + path + '-input');
    if (element) {
        element.value = data.get(path);
    }
}

function form_data_swap(path1, path2) {
    console.log('Swapping ', path1, path2);

    data_from_1 = data_form_get(path1);
    data_from_2 = data_form_get(path2);

    data_form_change(path1, data_from_2);
    data_form_change(path2, data_from_1);

    data_send(path1);
    data_send(path2);
}

function form_data_swap_script(script, param1, param2) {
    console.log('Swapping script ', script, param1, param2);

    data_from_1 = document.getElementById(script + "-" + param1).value;
    data_from_2 = document.getElementById(script + "-" + param2).value;

    document.getElementById(script + "-" + param2).value = data_from_1;
    document.getElementById(script + "-" + param1).value = data_from_2;
}

function form_upload_file(data_path) {
    let file = document.getElementById(data_path + '-file').files[0];
    let ajax = new XMLHttpRequest();
    ajax.upload.sg_data_path = data_path;
    ajax.sg_data_path = data_path;

    let form_data = new FormData();
    form_data.append('file', file);

    ajax.upload.addEventListener("progress", form_upload_file_progress, false);
    ajax.addEventListener('load', form_upload_file_load, false);
    ajax.open('POST', '/upload', true);
    ajax.send(form_data);

    let progress_save = document.getElementById(data_path + '-save');
    progress_save.disabled = true;
}

function form_upload_file_progress(event) {
    let progress_val = event.loaded / event.total * 100;

    let progress = document.getElementById(event.target.sg_data_path + '-progress');
    let progress_save = document.getElementById(event.target.sg_data_path + '-save');

    progress.classList.add('progress-bar-striped');
    progress.classList.add('progress-bar-animated');
    progress.classList.remove('bg-success');
    progress.classList.remove('bg-danger');
    progress.classList.add('bg-info');

    progress.ariaValueNow = progress_val;
    progress.style.width = progress_val + "%";

    progress_save.disabled = true;
}

function form_upload_file_load(event) {
    let progress = document.getElementById(event.target.sg_data_path + '-progress');
    let progress_info = document.getElementById(event.target.sg_data_path + '-info');
    let progress_path = document.getElementById(event.target.sg_data_path + '-path');
    let progress_save = document.getElementById(event.target.sg_data_path + '-save');

    if (event.target.status == 200) {
        progress.classList.remove('progress-bar-striped');
        progress.classList.remove('progress-bar-animated');
        progress.classList.remove('bg-info');
        progress.classList.remove('bg-danger');
        progress.classList.add('bg-success');

        progress_info.innerText = "File available at:";

        progress_path.innerText = event.target.responseText;
        progress_path.href = event.target.responseText;

        progress_save.disabled = false;
    } else if (event.target.status == 400) {
        progress.classList.add('progress-bar-striped');
        progress.classList.remove('progress-bar-animated');
        progress.classList.remove('bg-info');
        progress.classList.remove('bg-success');
        progress.classList.add('bg-danger');

        progress_info.innerText = "Error uploading file: " + event.target.responseText;

        progress_path.innerText = "";
        progress_path.href = "";

        progress_save.disabled = true;
    }
}

function form_upload_file_save(data_path) {
    let file_path = document.getElementById(data_path + '-path');
    let data_input_field = document.getElementById('data-' + data_path + '-input');

    data_input_field.value = file_path.innerText;
}

function form_file_list(data_path) {
    var request = new XMLHttpRequest();
    request.open('GET', '/api/v1/get_static_files', true);

    let file_list_div = document.getElementById(data_path + '-list');

    request.onload = function () {
        if (this.status >= 200 && this.status < 400) {
            let file_list = JSON.parse(this.response);

            file_list.forEach(function (file_path) {
                let form_check = document.createElement('div');
                form_check.classList.add('form-check');

                let radio = document.createElement('input');
                radio.classList.add('form-check-input');
                radio.type = 'radio';
                radio.name = data_path + '-pick';
                radio.id = data_path + file_path;
                radio.value = file_path;

                let label = document.createElement('label');
                label.classList.add('form-check-label');
                label.htmlFor = data_path + file_path;

                let external_link = document.createElement('a');
                external_link.href = file_path;
                external_link.target = "_blank";

                external_link.appendChild(window.FontAwesome.icon({
                    prefix: 'fas',
                    iconName: 'magnifying-glass'
                }).node[0]);
                label.appendChild(external_link);


                let image_preview = document.createElement('img');
                image_preview.src = file_path;
                //image_preview.style.height = "64px";
                image_preview.classList.add('img-fluid');
                image_preview.loading = "lazy";

                let tooltip = new bootstrap.Tooltip(external_link, {
                    "html": true,
                    "title": image_preview,
                    "placement": "right",
                });

                let description = document.createTextNode(" " + file_path);
                label.appendChild(description);

                form_check.appendChild(radio);
                form_check.appendChild(label);
                file_list_div.appendChild(form_check);
            });
        } else {
            if (window.console) console.log("Error downloading init data.");

        }
    };

    request.send();
}

function form_file_select(data_path) {
    let selected_file = document.querySelector('input[name="' + data_path + '-pick"]:checked').value;
    let data_input_field = document.getElementById('data-' + data_path + '-input');

    data_input_field.value = selected_file;
}


function run_editor_script(script_name) {
    console.log(script_name);
    console.log(editor_config_data['scripts'][script_name]);

    editor_config_data['scripts'][script_name]['actions'].forEach(function run_action(action) {
        if (action['action'] === "submit") {
            action['items'].forEach(function run_action_with(with_input) {
                data_send(with_input);
            });
        } else if (action['action'] === "helper-submit-all") {
            document.querySelectorAll('[id^="data-"],[id$="-input"]').forEach(input => {
                var path = input.id.substring("data-".length, input.id.length - "-input".length);

                data_send(path);
            });
        } else if (action['action'] === "plus-1") {
            action['items'].forEach(function run_action_with(with_input) {
                var curr_val = data_form_get(with_input);

                data_form_change(with_input, Number(curr_val) + 1);
                data_send(with_input);
            });
        } else if (action['action'] === "swap") {
            action['items'].forEach(function run_action_with(with_input, index) {
                form_data_swap(with_input, action['with'][index]);
            });
        } else if (action['action'] === "swap-script") {
            action['items'].forEach(function run_action_with(with_input, index) {
                form_data_swap_script(action['script'], with_input, action['with'][index]);
            });
            run_script(action['script']);
        }
    });
}

function formatParams(params) {
    return "?" + Object
        .keys(params)
        .map(function (key) {
            return key + "=" + encodeURIComponent(params[key]);
        })
        .join("&");
}