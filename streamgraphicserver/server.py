import datetime
import sys
import time
import gevent.monkey

gevent.monkey.patch_all()

import base64
import logging
import multiprocessing
import os
import random
import string
import traceback
from distutils.util import strtobool
from io import BytesIO
from threading import Thread
from urllib.parse import urljoin, urlparse

import bcrypt
import flask
import flask_login
import sass
from flask import Flask, jsonify, render_template, request, send_file
from flask_compress import Compress
from flask_executor import Executor
from flask_login import current_user, login_required, login_user
from flask_login.utils import logout_user
from flask_socketio import SocketIO
from flatten_json import unflatten
from simpleicons.all import icons as simpleicons
from werkzeug.utils import secure_filename

from streamgraphicserver.data_sources.data_sources import (
    autostart_data_sources,
    get_data_source_list,
    get_data_source_list_formatted,
    load_data_source_config,
    run_data_source_in_background,
    stop_data_source,
)
from streamgraphicserver.data_sources.net.counter_strike.game_state import parse_csgo_data
from streamgraphicserver.util.config import get_config
from streamgraphicserver.util.data import (
    REDIS_CONNECTION,
    REDIS_HOST,
    delete_keys,
    get_data,
    get_item,
    get_keys,
    get_log,
    get_mix_overlays,
    send_alert_overlay,
    send_data,
    send_data_multiple,
)
from streamgraphicserver.util.editor import get_editor_config, list_editor_properties
from streamgraphicserver.util.file_list import ALLOWED_EXTENSIONS, get_static_files
from streamgraphicserver.util.fontawesome import install_fontawesome
from streamgraphicserver.util.forms import LoginForm
from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.other import APP_VERSION, flatten
from streamgraphicserver.util.restore import (
    get_restore_properties_cache,
    get_restore_properties_cache_filtered,
    list_restore_file_names,
    list_restore_properties,
    render_templated_restore,
    run_script,
)
from streamgraphicserver.util.user import User

print("imports done.")

UPLOAD_FOLDER = "static/local/upload"

try:
    os.makedirs("streamgraphicserver/" + UPLOAD_FOLDER, exist_ok=True)

    multiprocessing.set_start_method("spawn")

    setup_loggers()

    app = Flask(__name__)
    app.secret_key = get_config().get(
        "config/system/secret_key",
        "".join(random.sample(string.ascii_lowercase + string.ascii_uppercase + string.digits, 16)),
    )
    app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER
    app.config["MAX_CONTENT_LENGTH"] = 25 * 1000 * 1000  # 25MB

    login_manager = flask_login.LoginManager()
    login_manager.init_app(app)

    executor = Executor(app)
    app.config["COMPRESS_REGISTER"] = False  # disable default compression of all eligible requests
    compress = Compress()
    compress.init_app(app)

    app.jinja_env.globals.update(get_data_value=get_item)

    logging.captureWarnings(True)
    logger = logging.getLogger(__name__)
    engineio_logger = logging.getLogger("engineio_logger")

    fontawesome_static_path = install_fontawesome(__name__, app, logger)

    sio = SocketIO(
        app,
        async_mode="gevent",
        message_queue=REDIS_HOST,
        engineio_logger=engineio_logger,
        cors_allowed_origins="*",
    )

    def allowed_file(filename):
        return "." in filename and filename.rsplit(".", 1)[1].lower() in ALLOWED_EXTENSIONS

    def is_safe_url(target):
        """
        https://github.com/fengsp/flask-snippets/blob/master/security/redirect_back.py
        """

        ref_url = urlparse(request.host_url)
        test_url = urlparse(urljoin(request.host_url, target))
        return test_url.scheme in ("http", "https") and ref_url.netloc == test_url.netloc

    @login_manager.user_loader
    def load_user(user_id):
        user = None

        config_key = "user/" + user_id
        if config_key in get_config().keys():
            user = User(username=user_id)

        return user

    @login_manager.request_loader
    def load_user_from_request(request):
        # print("Check API")
        # print('arg:', request.args.get("api_key"))
        # print('header:', request.headers.get("Authorization"))

        # first, try to login using the api_key url arg
        api_key = request.args.get("api_key")
        if api_key:
            # print("Has api_key arg")
            user = User.find_api(api_key=api_key)
            if user:
                # print("User found")
                return user

        # next, try to login using Basic Auth
        api_key = request.headers.get("Authorization")
        if api_key:
            # print("Has api_key header in Authorization")
            api_key = api_key.replace("Basic ", "", 1)
            try:
                api_key = base64.b64decode(api_key)
            except TypeError:
                pass
            user = User.find_api(api_key=api_key)
            if user:
                # print("User found")
                return user

        # finally, return None if both methods did not login the user
        return None

    @login_manager.unauthorized_handler
    def unauthorized_handler():
        next = flask.url_for(request.endpoint, **request.view_args)
        return flask.redirect(flask.url_for("serve_login", next=next))

    @app.route("/login", methods=["GET", "POST"])
    def serve_login():
        """For GET requests, display the login form.
        For POSTS, login the current user by processing the form.
        """
        form = LoginForm()
        if form.validate_on_submit():
            user = load_user(user_id=form.username.data)
            if user:
                if bcrypt.checkpw(form.password.data.encode("utf-8"), user.password.encode("utf-8")):
                    user.authenticated = True
                    login_user(user, remember=True)

                    flask.flash("Logged in successfully.")

                    next = form.next.data
                    print("NEXT:", next)
                    # is_safe_url should check if the url is safe for redirects.
                    # See http://flask.pocoo.org/snippets/62/ for an example.
                    if not is_safe_url(next):
                        return flask.abort(400)

                    return flask.redirect(next or flask.url_for("serve_index"))
                else:
                    print("user pass bad")
                    return flask.redirect(flask.url_for("serve_login"))
        else:
            print("no validate")
        return render_template(
            "login.jinja2",
            form=form,
            version=APP_VERSION,
            editors=list_editor_properties(),
        )

    @app.route("/user")
    @login_required
    def serve_user_info():
        return render_template("user_info.jinja2", version=APP_VERSION, editors=list_editor_properties())

    @app.route("/logout", methods=["GET"])
    @login_required
    def serve_logout():
        """Logout the current user."""
        user = current_user
        user.authenticated = False
        logout_user()
        return render_template("index.jinja2", version=APP_VERSION, editors=list_editor_properties())

    @app.route("/upload", methods=["POST"])
    @login_required
    def upload_file():
        # check if the post request has the file part
        if "file" not in request.files:
            return "no file", 400
        file = request.files["file"]
        # If the user does not select a file, the browser submits an
        # empty file without a filename.
        if file.filename == "":
            return "no file", 400
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join("streamgraphicserver/", app.config["UPLOAD_FOLDER"], filename))

            resp_filepath = "/" + app.config["UPLOAD_FOLDER"] + "/" + filename

            logger.info("Uploaded: " + resp_filepath)

            return resp_filepath, 200

        return "unknown error", 500

    @app.route("/")
    def serve_index():
        return render_template("index.jinja2", version=APP_VERSION, editors=list_editor_properties())

    @app.route("/overlay_list")
    def serve_overlay_list():
        return render_template("overlay_list.jinja2", version=APP_VERSION, editors=list_editor_properties())

    @app.route("/mix_list")
    def serve_mix_list():
        return render_template("mix_list.jinja2", version=APP_VERSION, editors=list_editor_properties())

    @app.route("/restore_list")
    @login_required
    def serve_restore_list():
        return render_template(
            "restore_list.jinja2",
            restore_properties=get_restore_properties_cache(),
            version=APP_VERSION,
            editors=list_editor_properties(),
        )

    @app.route("/restore_list/<path:restore_filter>")
    @login_required
    def serve_restore_list_filtered(restore_filter):
        return render_template(
            "restore_list.jinja2",
            restore_properties=get_restore_properties_cache_filtered(restore_filter),
            version=APP_VERSION,
            editors=list_editor_properties(),
        )

    @app.route("/script_list")
    @login_required
    def serve_script_list():
        return render_template(
            "script_list.jinja2",
            script_properties=get_data_source_list(),
            version=APP_VERSION,
            editors=list_editor_properties(),
        )

    @app.route("/alert_overlay/<alert_channel>")
    def serve_alert_overlay(alert_channel):
        return render_template("alert_overlay.jinja2", alert_channel=alert_channel, version=APP_VERSION)

    @app.route("/layer/<layer_name>")
    def serve_layer(layer_name):
        return render_template("layer.jinja2", layer_name=layer_name, version=APP_VERSION)

    @app.route("/mix/<mix_name>")
    def serve_mix(mix_name):
        overlay_list = get_mix_overlays(mix_name)

        return render_template("mix.jinja2", overlay_list=overlay_list, version=APP_VERSION)

    @app.route("/mix_bg/<mix_name>")
    def serve_mix_bg(mix_name):
        color = request.args.get("color", default="200000", type=str)
        overlay_list = get_mix_overlays(mix_name)

        return render_template(
            "mix_bg.jinja2",
            overlay_list=overlay_list,
            version=APP_VERSION,
            color=color,
        )

    @app.route("/mix_fallback/<mix_name>")
    def serve_mix_fallback(mix_name):
        overlay_list = get_mix_overlays(mix_name)

        return render_template(
            "mix.jinja2",
            overlay_list=overlay_list,
            version=APP_VERSION,
            fallback_mode=True,
        )

    @app.route("/mix_bg_fallback/<mix_name>")
    def serve_mix_bg_fallback(mix_name):
        color = request.args.get("color", default="200000", type=str)
        overlay_list = get_mix_overlays(mix_name)

        return render_template(
            "mix_bg.jinja2",
            overlay_list=overlay_list,
            version=APP_VERSION,
            color=color,
            fallback_mode=True,
        )

    @app.route("/overlay/<overlay_name>")
    def serve_overlay(overlay_name):
        return render_template("overlay.jinja2", overlay_name=overlay_name, version=APP_VERSION)

    @app.route("/overlay_bg/<overlay_name>")
    def serve_overlay_bg(overlay_name):
        color = request.args.get("color", default="200000", type=str)

        return render_template(
            "overlay_bg.jinja2",
            overlay_name=overlay_name,
            version=APP_VERSION,
            color=color,
        )

    @app.route("/overlay_fallback/<overlay_name>")
    def serve_overlay_fallback(overlay_name):
        return render_template(
            "overlay.jinja2",
            overlay_name=overlay_name,
            version=APP_VERSION,
            fallback_mode=True,
        )

    @app.route("/overlay_bg_fallback/<overlay_name>")
    def serve_overlay_bg_fallback(overlay_name):
        color = request.args.get("color", default="200000", type=str)

        return render_template(
            "overlay_bg.jinja2",
            overlay_name=overlay_name,
            version=APP_VERSION,
            color=color,
            fallback_mode=True,
        )

    @app.route("/overlay_bg_move/<overlay_name>")
    def serve_overlay_bg_move(overlay_name):
        color = request.args.get("color", default="200000", type=str)
        seed = request.args.get("seed", default="osu-rand-sm", type=str)
        generator = request.args.get("generator", default="squares", type=str)

        return render_template(
            "overlay_bg_motion.jinja2",
            overlay_name=overlay_name,
            version=APP_VERSION,
            color=color,
            seed=seed,
            generator=generator,
        )

    @app.route("/raw_edit/<path:path>")
    @login_required
    def serve_data(path):
        return render_template("edit.jinja2", path=path)

    @app.route("/editor/<path:template_path>/<template_name>")
    @login_required
    def serve_editor_template(template_path, template_name):
        return render_template(
            "editor_template.jinja2",
            editor_config=get_editor_config(template_path, template_name)["page"],
            script_properties=get_data_source_list(),
            version=APP_VERSION,
            editors=list_editor_properties(),
            curr_config=get_config(),
        )

    @app.route("/editor_nohead/<path:template_path>/<template_name>")
    @login_required
    def serve_editor_nonavi_template(template_path, template_name):
        return render_template(
            "editor_template.jinja2",
            editor_config=get_editor_config(template_path, template_name)["page"],
            script_properties=get_data_source_list(),
            version=APP_VERSION,
            editors=list_editor_properties(),
            curr_config=get_config(),
            disable_header=True,
        )

    @app.route("/api/v1/alert_overlay/<alert_channel>", methods=["POST"])
    @login_required
    def serve_api_alert_overlay_display(alert_channel):
        text_right = request.args.get("text_right", default="", type=str)
        text_left = request.args.get("text_left", default="", type=str)
        image = request.args.get("image", default="", type=str)
        fa_icon = request.args.get("fa_icon", default="", type=str)
        separator = request.args.get("separator", default=": ", type=str)
        bg_color = request.args.get("bg_color", default="black", type=str)
        text_color = request.args.get("text_color", default="white", type=str)
        display_time = request.args.get("time", default=5000, type=int)

        send_alert_overlay(
            alert_channel=alert_channel,
            text_right=text_right,
            text_left=text_left,
            image=image,
            fa_icon=fa_icon,
            separator=separator,
            bg_color=bg_color,
            text_color=text_color,
            time=display_time,
        )

        return "", 200

    @app.route("/api/v1/export")
    @compress.compressed()
    def serve_api_data():
        return jsonify(get_data())

    @app.route("/api/v1/search/<path:search>")
    @compress.compressed()
    def serve_api_search(search):
        return jsonify(get_data("*" + search + "*"))

    @app.route("/api/v1/search_prefix/<path:search>")
    @compress.compressed()
    def serve_api_search_prefix(search):
        return jsonify(get_data(search + "/*"))

    @app.route("/api/v1/search_prefix_unflatten/<path:search>")
    @compress.compressed()
    def serve_api_search_prefix_unflatten(search):
        return jsonify(unflatten(get_data(search + "/*"), separator="/"))

    @app.route("/api/v1/mix_list")
    def serve_api_mix_list():
        keys = get_keys("mix/*")

        data = []

        for key in keys:
            name = key.split("/")[1]
            if name not in data:
                data.append(name)

        return jsonify(data)

    @app.route("/api/v1/overlay_list")
    def serve_api_overlay_list():
        keys = get_keys("overlay/*")

        data = []

        for key in keys:
            name = key.split("/")[1]
            if name not in data:
                data.append(name)

        return jsonify(data)

    @app.route("/api/v1/layer_list")
    def serve_api_layer_list():
        keys = get_keys("layer/*")

        data = []

        for key in keys:
            name = key.split("/")[1]
            if name not in data:
                data.append(name)

        return jsonify(data)

    @app.route("/api/v1/restore_list")
    def serve_api_restore_list():
        return jsonify(list_restore_file_names())

    @app.route("/api/v1/restore_properties")
    def serve_api_restore_properties():
        return jsonify(get_restore_properties_cache())

    @app.route("/api/v1/editor_properties")
    def serve_api_editor_properties():
        return jsonify(list_editor_properties())

    @app.route("/api/v1/reload_restore_properties", methods=["POST"])
    @login_required
    def serve_api_reload_restore_properties():

        executor.submit(list_restore_properties)

        return "", 200

    @app.route("/api/v1/get/<path:path>")
    def serve_api_get(path):
        return str(REDIS_CONNECTION.get(path))

    @app.route("/api/v1/set/<path:path>", methods=["POST"])
    @login_required
    def serve_api_set(path):
        send_data(path, request.form["value"])
        return "", 200

    @app.route("/api/v1/clear/<path:path>", methods=["DELETE"])
    @login_required
    def serve_api_clear(path):
        delete_keys(search=path)
        return "", 200

    @app.route("/api/v1/easy_set/<path:path>/<value>", methods=["POST"])
    @login_required
    def serve_api_set_easy(path, value):
        def do_work(path, value):
            send_data(path, value)

        thread = Thread(target=do_work, kwargs={"path": path, "value": value}, daemon=True)
        thread.start()
        return "", 200

    @app.route("/api/v1/set_multiple", methods=["POST"])
    @login_required
    def serve_api_set_multiple():
        def do_work(json):
            send_data_multiple(json)

        thread = Thread(target=do_work, kwargs={"json": request.get_json(force=True)}, daemon=True)
        thread.start()
        return "", 200

    @app.route("/api/v1/set_multiple_for_path_and_flatten/<path:path>", methods=["POST"])
    @login_required
    def serve_api_set_multiple_for_path_and_flatten(path):
        def do_work(json, path):
            send_data_multiple(flatten(json, parent_key=path))

        thread = Thread(
            target=do_work,
            kwargs={"json": request.get_json(force=True), "path": path},
            daemon=True,
        )
        thread.start()
        return "", 200

    @app.route("/api/v1/restore_debug/<path:path>")
    def serve_api_debug_template_restore(path):
        (data, _) = render_templated_restore("restore/" + path + ".yaml.j2", variables=request.args)

        return jsonify(data)

    @app.route("/api/v1/restore_default/<path:path>", methods=["POST"])
    @login_required
    def serve_api_restore_default(path):
        def do_work(path, variables):
            run_script("restore/" + path, None, variables=variables)

        thread = Thread(
            target=do_work,
            kwargs={"path": path, "variables": request.args},
            daemon=True,
        )
        thread.start()
        return "", 200

    @app.route("/api/v1/restore/<path:path>/<value>", methods=["POST"])
    @login_required
    def serve_api_restore(path, value):

        thread = Thread(
            target=run_script,
            kwargs={
                "restore_filename": "restore/" + path,
                "script_name": value,
                "variables": request.args,
            },
            daemon=True,
        )
        thread.start()
        return "", 200

    @app.route("/api/v1/list_data_sources")
    def serve_api_list_data_sources():
        return jsonify(get_data_source_list_formatted())

    @app.route("/api/v1/run_data_source/<path:path>", methods=["POST", "DELETE"])
    @login_required
    def serve_api_run_data_source(path):
        if request.method == "POST":
            logger.info("Requested data_source run: %s - %s", path, request.get_json(force=True))
            run_data_source_in_background(path, **request.get_json(force=True))
            return "", 200
        elif request.method == "DELETE":
            logger.info("Stopping data_source: %s", path)
            stop_data_source(path)
            return "", 200

    @app.route("/api/v1/get_log")
    @compress.compressed()
    def serve_api_get_log():
        log_name = request.args.get("log_name", default="data_log", type=str)
        start = request.args.get("start", default="-", type=str)
        end = request.args.get("end", default="+", type=str)

        return jsonify(
            [{"time": time.split("-")[0], "id": time, "data_updates": data_updates} for time, data_updates in get_log(log=log_name, start=start, end=end)]
        )

    @app.route("/api/v1/icons/simpleicons/<icon_name>")
    @compress.compressed()
    def serve_simpleicons_image(icon_name):
        # bool doesn't convert well... :(
        color = strtobool(request.args.get("color", default="false", type=str))
        fill = request.args.get("fill", default=None, type=str)

        if icon_name not in simpleicons:
            icon_name = "simpleicons"

        settings = {}

        if color:
            settings["fill"] = "#" + simpleicons[icon_name].hex
        elif fill:
            settings["fill"] = "#" + fill

        icon_xml = simpleicons[icon_name].get_xml_bytes(**settings)

        return send_file(BytesIO(icon_xml), mimetype="image/svg+xml")

    @app.route("/api/v1/get_static_files")
    def serve_api_list_static():
        return jsonify(get_static_files())

    @app.route("/api/v1/webcaptioner/<track>", methods=["POST"])
    def serve_api_webcaptioner(track):
        data = request.get_json(force=True)

        print(track, data["sequence"], data["transcript"])

        return "", 200

    @app.route("/gamestate/csgo", methods=["POST"])
    def serve_gamestate_csgo():
        data = request.json

        # if 'previously' in data:
        #     del data['previously']
        # if 'added' in data:
        #     del data['added']
        # if 'grenades' in data:
        #     del data['grenades']
        # if 'provider' in data:
        #     del data['provider']

        # send_data_multiple(flatten(data, parent_key='data/net/counter-strike/game-state-raw'))

        parsed_data = parse_csgo_data(data)

        send_data_multiple(flatten(parsed_data, parent_key="data/net/counter-strike/game-state"))

        return "", 200, {"Content-Type": "text/html"}

    @sio.on("connect", namespace="/websocket")
    def sio_connect():
        logger.info("socket_io connect")

    @sio.on("disconnect", namespace="/websocket")
    def sio_disconnect():
        logger.info("socket_io disconnect")

    logger.info("Running SASS compile...")
    try:
        print(__package__)
        sass.compile(
            dirname=("streamgraphicserver/static/sass", "streamgraphicserver/static/css"),
            output_style="compressed",
        )
    except:
        traceback.print_exc()
        sys.exit(1)
    logger.info("Completed SASS compile.")

    logger.info("Running cleanup process...")

    # delete_keys(search="data/net/counter-strike/game-state/*")
    delete_keys(search="data/net/counter-strike/game-state-raw/*")
    delete_keys(search="data/com/indycar/ts*")
    delete_keys(search="data/com/nascar/*")
    # delete_keys(search="data/com/rocketleague/gsi/*")
    # delete_keys(search="data/net/minecraft/*")
    delete_keys(search="data/gg/smash/smashgg/monitored_phase/*")
    delete_keys(search="data/io/github/librehardwaremonitor/*")

    logger.info("Cleanup process complete.")

    logger.info("Loading saved configuration...")
    try:
        load_data_source_config()
    except:
        traceback.print_exc()
    logger.info("Loaded saved configuration.")

    logger.info("Autostart default data sources.")
    try:
        autostart_data_sources()
    except:
        traceback.print_exc()

    start_time = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc).isoformat()
    start_unix = int(time.time())
    start_redis = start_unix * 1000
    send_data_multiple(flatten({"last_launch": start_time, "last_launch_unix": start_unix, "last_launch_redis": start_redis}, parent_key="data/system/server"))
    logger.info("Server is now running.")
except:
    traceback.print_exc()
