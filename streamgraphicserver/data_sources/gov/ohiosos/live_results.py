import datetime
import logging
import multiprocessing
import sched
import time
import pytz

import requests
import cloudscraper

from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.other import flatten
from streamgraphicserver.util.remote_data import send_data, send_data_multiple

scraper = cloudscraper.create_scraper(interpreter="nodejs")

SESSION = requests.Session()

# HEADERS = {"user-agent": "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Mobile Safari/537.36"}


def div_ignore_0(n, d):
    return (n / d) * 100 if d else 0


def update_live_ohio_votes(scheduler=None):
    logger = logging.getLogger(__name__)

    overall_stats = scraper.get("https://liveresults.ohiosos.gov/Api/v1/Contest/getBallotsCastByCounty").json()[0]

    if overall_stats["objectId"] != None and overall_stats["name"] != "Totals":
        logger.warning("WARNING - WARNING - WARNING: API CODE SHORTCUT NOT WORKING/UNEXPECTED")

    send_data(data=overall_stats["precinctReportingPercNum"], path="data/gov/ohiosos/live_results/precints_reporting_precent")

    statewide_issues = scraper.get(
        "https://liveresults.ohiosos.gov/Api/v1/Contest/GetContestDetailsObject?contestName=Statewide%20Issues&contestObjectId=&resultsLevel=&countyId="
    ).json()["contestList"]

    for contest in statewide_issues:
        contest_info_processed = {"name": contest["name"], "canidates": [], "remaining_vote_percent": 100 - overall_stats["precinctReportingPercNum"]}

        for canidate in contest["candidatesRunning"]:
            contest_info_processed["canidates"].append(
                {
                    "name": canidate["primaryCandidate"],
                    "vote_percent": canidate["votePercentageNum"],
                    "vote_percent_enhanced": canidate["votePercentageNum"] * overall_stats["precinctReportingPercNum"],
                }
            )

        send_data_multiple(
            flatten(
                contest_info_processed,
                parent_key="data/gov/ohiosos/live_results/contests/" + contest["objectId"],
            )
        )

    now = datetime.datetime.now(pytz.timezone("US/Eastern"))
    send_data(data=now.strftime("%I:%M:%S %p").lstrip("0"), path="data/gov/ohiosos/live_results/last_update_time")

    logger.info("Data refreshed.")

    if scheduler:
        scheduler.enter(30, 1, update_live_ohio_votes, argument=(scheduler,))


def run():
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Starting gov/ohiosos/live_results data_source")

    # delete_keys(search="data/gov/ohiosos/live_results/*")

    scheduler = sched.scheduler(time.time, time.sleep)

    scheduler.enter(0, 1, update_live_ohio_votes, argument=(scheduler,))
    scheduler.run()


if __name__ == "__main__":
    s = sched.scheduler(time.time, time.sleep)

    s.enter(0, 1, update_live_ohio_votes, argument=(s,))
    s.run()
