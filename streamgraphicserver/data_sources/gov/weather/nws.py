import logging
import multiprocessing
import sched
import time
import traceback
import gevent

import requests
from streamgraphicserver.util.remote_data import delete_keys, send_data_multiple
from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.other import flatten

test_mode = True


def update_weather(lat, long, location_name=None, scheduler=None):
    logger = logging.getLogger(__name__)

    if not location_name:
        location_name = str(lat) + "," + str(long)

    try:
        meta = requests.get(
            "https://api.weather.gov/points/" + str(lat) + "," + str(long),
            timeout=(3.05, 27),
        )
        forecast = requests.get(meta.json()["properties"]["forecast"], timeout=(3.05, 27))
        forecast_hourly = requests.get(meta.json()["properties"]["forecastHourly"], timeout=(3.05, 27))
        forecast_grid_data = requests.get(meta.json()["properties"]["forecastGridData"], timeout=(3.05, 27))

        easy_data = requests.get(
            "https://forecast.weather.gov/MapClick.php?lat=" + str(lat) + "&lon=" + str(long) + "&unit=0&lg=english&FcstType=json",
            timeout=(3.05, 27),
        )

        alerts = requests.get(
            "https://api.weather.gov/alerts/active?status=actual&message_type=alert&point=" + str(lat) + "," + str(long),
            timeout=(3.05, 27),
        )

        delete_keys(search="data/gov/weather/" + location_name + "/*")

        if alerts.json().get("features"):
            send_data_multiple(
                flatten(
                    alerts.json()["features"],
                    parent_key="data/gov/weather/" + location_name + "/alerts/zone",
                )
            )
        else:
            logger.info(
                "No alerts for %s",
                "https://api.weather.gov/alerts/active?status=actual&message_type=alert&point=" + str(lat) + "," + str(long),
            )

        if forecast.json().get("properties"):
            send_data_multiple(
                flatten(
                    forecast.json()["properties"],
                    parent_key="data/gov/weather/" + location_name + "/forecast",
                )
            )
        else:
            logger.warn("No valid forcast data for %s", meta.json()["properties"]["forecast"])

        if forecast_hourly.json().get("properties"):
            send_data_multiple(
                flatten(
                    forecast_hourly.json()["properties"],
                    parent_key="data/gov/weather/" + location_name + "/forecast_hourly",
                )
            )
        else:
            logger.warn(
                "No valid forcast data for %s",
                meta.json()["properties"]["forecastHourly"],
            )

        if forecast_grid_data.json().get("properties"):
            send_data_multiple(
                flatten(
                    forecast_grid_data.json()["properties"],
                    parent_key="data/gov/weather/" + location_name + "/forecast_grid_data",
                )
            )

            chart_data = {
                "probabilityOfPrecipitation": {"x": "", "y": ""},
                "temperature": {"x": "", "y": ""},
                "apparentTemperature": {"x": "", "y": ""},
            }

            for type_data in chart_data.keys():
                x_data = []
                y_data = []

                for forcast_data in forecast_grid_data.json()["properties"][type_data]["values"]:
                    x_data.append(forcast_data["validTime"].split("/")[0])

                    if (
                        "uom" in forecast_grid_data.json()["properties"][type_data].keys()
                        and forecast_grid_data.json()["properties"][type_data]["uom"] == "wmoUnit:degC"
                    ):
                        y_data.append((forcast_data["value"] * 1.8) + 32)
                    else:
                        y_data.append(forcast_data["value"])

                chart_data[type_data]["x"] = ",".join(map(str, x_data))
                chart_data[type_data]["y"] = ",".join(map(str, y_data))

            send_data_multiple(flatten(chart_data, parent_key="data/gov/weather/" + location_name + "/charts"))
        else:
            logger.warn(
                "No valid forcast data for %s",
                meta.json()["properties"]["forecastGridData"],
            )

        if easy_data.json():
            send_data_multiple(
                flatten(
                    easy_data.json(),
                    parent_key="data/gov/weather/" + location_name + "/easy_data",
                )
            )
        else:
            logger.warn(
                "No valid easy data for %s",
                "https://forecast.weather.gov/MapClick.php?lat=" + str(lat) + "&lon=" + str(long) + "&unit=0&lg=english&FcstType=json",
            )

    except:
        traceback.print_exc()

    if scheduler:
        scheduler.enter(30, 1, update_weather, argument=(lat, long, location_name, scheduler))


def run(lat=39.999436, long=-83.012724):
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Starting gov/weather/nws data_source: lat=%s, long=%s", lat, long)
    scheduler = sched.scheduler(time.time, gevent.sleep)

    scheduler.enter(0, 1, update_weather, argument=(lat, long, None, scheduler))
    scheduler.run()
