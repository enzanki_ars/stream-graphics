import copy
import datetime
import json
import logging
from multiprocessing import Process, get_context

import streamgraphicserver.data_sources.com.coinbase.coinbase_pro_market
import streamgraphicserver.data_sources.com.coinbase.coinbase_pro_ws
import streamgraphicserver.data_sources.com.imsa.imsa_live_tc
import streamgraphicserver.data_sources.com.indycar.indycar_live_tc
import streamgraphicserver.data_sources.com.indycar.indycar_websocket_tc
import streamgraphicserver.data_sources.com.mlb.live_game
import streamgraphicserver.data_sources.com.nascar.nascar_live_tc
import streamgraphicserver.data_sources.com.nasdaq.market
import streamgraphicserver.data_sources.com.ncaa.mm
import streamgraphicserver.data_sources.com.ncaa.scores
import streamgraphicserver.data_sources.com.open_metro.forecast
import streamgraphicserver.data_sources.com.rocketleague.color
import streamgraphicserver.data_sources.com.twitter.tweet
import streamgraphicserver.data_sources.edu.osu.buckeyethon
import streamgraphicserver.data_sources.edu.osu.emergency
import streamgraphicserver.data_sources.gg.smash.monitor_phase_entrants
import streamgraphicserver.data_sources.gg.smash.tournament_stream
import streamgraphicserver.data_sources.gov.frankincounty.vote
import streamgraphicserver.data_sources.gov.ohiosos.live_results
import streamgraphicserver.data_sources.gov.weather.nws
import streamgraphicserver.data_sources.io.github.enzanki_ars.home
import streamgraphicserver.data_sources.io.github.enzanki_ars.status
import streamgraphicserver.data_sources.io.gitlab.osubgc.results
import streamgraphicserver.data_sources.org.extralife.donations
import streamgraphicserver.data_sources.system.time.countdown
import streamgraphicserver.data_sources.system.time.current_time
import streamgraphicserver.data_sources.system.vlc.now_playing

data_source_list = {
    "com/twitter/tweet": {
        "run": streamgraphicserver.data_sources.com.twitter.tweet.run,
        "parameters": {
            "tweet": {
                "default": "",
                "path": "config/com/twitter/tweet/tweet",
                "documentation": "Tweet number (last part of URL).",
            },
            "consumer_key": {
                "default": "",
                "path": "config/com/twitter/tweet/consumer_key",
                "documentation": "",
                "hidden": True,
            },
            "consumer_secret": {
                "default": "",
                "path": "config/com/twitter/tweet/consumer_secret",
                "documentation": "",
                "hidden": True,
            },
            "access_token_key": {
                "default": "",
                "path": "config/com/twitter/tweet/access_token_key",
                "documentation": "",
                "hidden": True,
            },
            "access_token_secret": {
                "default": "",
                "path": "config/com/twitter/tweet/access_token_secret",
                "documentation": "",
                "hidden": True,
            },
        },
    },
    "com/ncaa/scores": {
        "run": streamgraphicserver.data_sources.com.ncaa.scores.run,
        "parameters": {
            "game": {
                "default": "",
                "path": "config/com/ncaa/scores/game",
                "documentation": "Game number (last part of URL).",
            }
        },
    },
    "com/ncaa/mm": {
        "run": streamgraphicserver.data_sources.com.ncaa.mm.run,
        "parameters": {
            "group": {
                "default": "",
                "path": "config/com/ncaa/mm/bracket/group",
                "documentation": "Group number (last part of URL).",
            }
        },
    },
    "com/coinbase/coinbase_pro_market": {
        "run": streamgraphicserver.data_sources.com.coinbase.coinbase_pro_market.run,
        "parameters": {},
    },
    "com/coinbase/coinbase_pro_ws": {
        "run": streamgraphicserver.data_sources.com.coinbase.coinbase_pro_ws.run,
        "parameters": {},
    },
    "com/indycar/tc": {
        "run": streamgraphicserver.data_sources.com.indycar.indycar_live_tc.run,
        "parameters": {},
    },
    "com/indycar/tc_websocket": {
        "run": streamgraphicserver.data_sources.com.indycar.indycar_websocket_tc.run,
        "parameters": {},
    },
    "com/imsa/tc": {
        "run": streamgraphicserver.data_sources.com.imsa.imsa_live_tc.run,
        "parameters": {},
    },
    "com/mlb/live_game": {
        "run": streamgraphicserver.data_sources.com.mlb.live_game.run,
        "parameters": {
            "game": {
                "default": "",
                "path": "config/com/mlb/live_game/game",
                "documentation": "Game number (last part of URL, number).",
            }
        },
    },
    "com/nascar/tc": {
        "run": streamgraphicserver.data_sources.com.nascar.nascar_live_tc.run,
        "parameters": {},
    },
    "com/nasdaq/tc": {
        "run": streamgraphicserver.data_sources.com.nasdaq.market.run,
        "parameters": {},
    },
    "com/open-metro/forecast": {
        "run": streamgraphicserver.data_sources.com.open_metro.forecast.run,
        "parameters": {
            "lat": {
                "default": "",
                "path": "config/gov/weather/nws/lat",
                "documentation": "",
            },
            "long": {
                "default": "",
                "path": "config/gov/weather/nws/long",
                "documentation": "",
            },
        },
    },
    "com/rocketleague/colors": {
        "run": streamgraphicserver.data_sources.com.rocketleague.color.run,
        "parameters": {
            "bakkesmod_server": {
                "default": "localhost",
                "path": "config/com/rocketleague/bakkesmod_server",
                "documentation": "",
            },
            "team0_primary": {
                "default": "",
                "path": "config/com/rocketleague/colors/team0/primary",
                "documentation": "",
            },
            "team0_secondary": {
                "default": "",
                "path": "config/com/rocketleague/colors/team0/secondary",
                "documentation": "",
            },
            "team1_primary": {
                "default": "",
                "path": "config/com/rocketleague/colors/team1/primary",
                "documentation": "",
            },
            "team1_secondary": {
                "default": "",
                "path": "config/com/rocketleague/colors/team1/secondary",
                "documentation": "",
            },
            "rcon_password": {
                "default": "",
                "path": "config/com/rocketleague/bakkesmod_rcon_password",
                "documentation": "",
                "hidden": True,
            },
        },
    },
    "edu/osu/buckeyethon": {
        "run": streamgraphicserver.data_sources.edu.osu.buckeyethon.run,
        "parameters": {
            "api_key": {
                "default": "",
                "path": "config/edu/osu/buckeyethon/api_key",
                "documentation": "Teamraiser API key.",
                "hidden": True,
            },
            "url": {
                "default": "",
                "path": "config/edu/osu/buckeyethon/url",
                "documentation": "Teamraiser URL.",
                "hidden": True,
            },
            "team_id": {
                "default": "",
                "path": "config/edu/osu/buckeyethon/team_id",
                "documentation": "Team ID from Teamraiser.",
            },
        },
    },
    "edu/osu/emergency": {
        "run": streamgraphicserver.data_sources.edu.osu.emergency.run,
        "parameters": {
            "test_mode": {
                "default": "",
                "path": "config/edu/osu/emergency/test_mode",
                "documentation": "Leave blank for normal mode. Enter anything to start TEST mode.",
            }
        },
    },
    "gg/smash/monitor_phase_entrants": {
        "run": streamgraphicserver.data_sources.gg.smash.monitor_phase_entrants.run,
        "parameters": {
            "phaseGroupId": {
                "default": "",
                "path": "config/gg/smash/smashgg/phaseGroupId",
                "documentation": "ID of the group to monitor.",
            },
            "monitoredEntrants": {
                "default": "",
                "path": "config/gg/smash/smashgg/monitoredEntrants",
                "documentation": "Comma separated list of entrants to monitor.",
            },
            "smashgg_api_key": {
                "default": "",
                "path": "config/gg/smash/smashgg/api_key",
                "documentation": "",
                "hidden": True,
            },
        },
    },
    "gg/smash/tournament_stream": {
        "run": streamgraphicserver.data_sources.gg.smash.tournament_stream.run,
        "parameters": {
            "tournament_name": {
                "default": "",
                "path": "config/gg/smash/smashgg/tournament_name",
                "documentation": "Part of the URL, for example 'tournament/testing-enzanki_ars'.",
            },
            "smashgg_api_key": {
                "default": "",
                "path": "config/gg/smash/smashgg/api_key",
                "documentation": "",
                "hidden": True,
            },
        },
    },
    "gov/franklincounty/vote": {
        "run": streamgraphicserver.data_sources.gov.frankincounty.vote.run,
        "parameters": {},
    },
    "gov/ohiosos/vote": {
        "run": streamgraphicserver.data_sources.gov.ohiosos.live_results.run,
        "parameters": {},
    },
    "gov/weather/nws": {
        "run": streamgraphicserver.data_sources.gov.weather.nws.run,
        "parameters": {
            "lat": {
                "default": "",
                "path": "config/gov/weather/nws/lat",
                "documentation": "",
            },
            "long": {
                "default": "",
                "path": "config/gov/weather/nws/long",
                "documentation": "",
            },
        },
    },
    "io/github/enzanki_ars/home": {
        "run": streamgraphicserver.data_sources.io.github.enzanki_ars.home.run,
        "parameters": {
            "lat": {
                "default": "",
                "path": "config/io/github/enzanki-ars/home/lat",
                "documentation": "",
                "hidden": True,
            },
            "long": {
                "default": "",
                "path": "config/io/github/enzanki-ars/home/long",
                "documentation": "",
                "hidden": True,
            },
        },
    },
    "io/github/enzanki_ars/status": {
        "run": streamgraphicserver.data_sources.io.github.enzanki_ars.status.run,
        "parameters": {},
    },
    "io/gitlab/osubgc/results": {
        "run": streamgraphicserver.data_sources.io.gitlab.osubgc.results.run,
        "parameters": {},
    },
    "org/extra-life/donations": {
        "run": streamgraphicserver.data_sources.org.extralife.donations.run,
        "parameters": {
            "team": {
                "default": 52335,
                "path": "config/org/extra-life/donations/team",
                "documentation": "Team number from Extra Life page URL.",
            }
        },
    },
    "system/vlc/now_playing": {
        "run": streamgraphicserver.data_sources.system.vlc.now_playing.run,
        "parameters": {
            "addr": {
                "default": "http://127.0.0.1:8000",
                "path": "config/system/vlc/now_playing/addr",
                "documentation": "URL of the VLC host.",
            },
            "password": {
                "default": "stream",
                "path": "config/system/vlc/now_playing/password",
                "documentation": "Password of the VLC HTTP server.",
                "hidden": True,
            },
        },
    },
    "system/time/countdown": {
        "run": streamgraphicserver.data_sources.system.time.countdown.run,
        "parameters": {
            "countdown": {
                "default": str(datetime.datetime.now().replace(second=0, microsecond=0).isoformat()),
                "path": "config/system/time/countdown/countdown",
                "type": "datetime-local",
                "documentation": "Countdown to.",
            }
        },
    },
    "system/time/current_time": {
        "run": streamgraphicserver.data_sources.system.time.current_time.run,
        "parameters": {},
        "autostart": True,
    },
}


for data_source_name, data_source_info in data_source_list.items():
    data_source_info["process"] = None
    data_source_info["run_parameters"] = {}
    data_source_info["has_term_once"] = False


def get_data_source_list():
    return data_source_list


def get_data_source_config():
    config = {}

    for data_source in data_source_list:
        for parameter in data_source_list[data_source]["parameters"]:
            if data_source_list[data_source]["run_parameters"].get(parameter):
                config[data_source_list[data_source]["parameters"][parameter]["path"]] = data_source_list[data_source]["run_parameters"][parameter]
            else:
                config[data_source_list[data_source]["parameters"][parameter]["path"]] = data_source_list[data_source]["parameters"][parameter]["default"]

    return config


def save_data_source_config():
    with open("config.json", "r") as json_data:
        config = json.load(json_data)

    curr_config = get_data_source_config()
    not_seen = list(config.keys())

    for data_source in data_source_list:
        for parameter in data_source_list[data_source]["parameters"]:
            parameter_def = data_source_list[data_source]["parameters"][parameter]
            if parameter_def["path"] in not_seen:
                not_seen.remove(parameter_def["path"])

    readd = {}

    for not_seen_entry in not_seen:
        readd[not_seen_entry] = config[not_seen_entry]

    new_config = {**curr_config, **readd}

    with open("config.json", "w") as json_data_new:
        json.dump(new_config, json_data_new, indent=4)


def load_data_source_config():
    with open("config.json") as json_data:
        config = json.load(json_data)

        for data_source in data_source_list:
            for parameter in data_source_list[data_source]["parameters"]:
                parameter_def = data_source_list[data_source]["parameters"][parameter]
                if parameter_def["path"] in config:
                    data_source_list[data_source]["parameters"][parameter]["default"] = config[parameter_def["path"]]


def get_data_source_list_formatted():
    data_source_list_edited = {}

    for data_name, data_source in data_source_list.items():
        data_source_list_edited[data_name] = {
            "parameters": {
                param_name: param_settings for param_name, param_settings in data_source["parameters"].items() if not param_settings.get("hidden", False)
            },
            "run_parameters": {
                param_name: param_settings for param_name, param_settings in data_source["run_parameters"].items() if not param_settings.get("hidden", False)
            },
            "process": (data_source["process"] is not None and data_source["process"].is_alive()),
            "has_term_once": data_source["has_term_once"],
        }

    return data_source_list_edited


def return_default_parameters(data_source):
    defaults = {}

    for parameter in data_source_list[data_source]["parameters"]:
        defaults[parameter] = data_source_list[data_source]["parameters"][parameter]["default"]

    return defaults


def override_parameters(data_source, **kwargs):
    options = return_default_parameters(data_source)

    for key, value in kwargs.items():
        options[key] = value

    return options


def autostart_data_sources():
    for data_source in data_source_list:
        if data_source_list[data_source].get("autostart", False):
            start_data_source_in_background(data_source)


def run_data_source(data_source, **kwargs):
    data_source_list[data_source]["run"](**override_parameters(data_source, **kwargs))
    data_source_list[data_source]["has_term_once"] = False


def stop_data_source(data_source):
    data_logger = logging.getLogger(__name__)

    # Process still tracked, online, and not killed once.
    if (
        data_source_list[data_source]["process"] is not None
        and data_source_list[data_source]["process"].is_alive()
        and not data_source_list[data_source]["has_term_once"]
    ):
        data_logger.info("Process for %s is being terminated.", data_source)
        data_source_list[data_source]["process"].terminate()
        data_source_list[data_source]["process"].join()
        data_logger.info("Process for %s has been stopped.", data_source)
        data_source_list[data_source]["has_term_once"] = True
    # Process still tracked, online, and killed once.
    elif (
        data_source_list[data_source]["process"] is not None
        and data_source_list[data_source]["process"].is_alive()
        and data_source_list[data_source]["has_term_once"]
    ):
        data_logger.info("Process for %s has already been stopped. Forcefully killing.", data_source)
        data_source_list[data_source]["process"] = None
        data_source_list[data_source]["has_term_once"] = False
    # Process not tracked, or not online, or killed twice.
    else:
        data_source_list[data_source]["has_term_once"] = False
        data_logger.info("Process for %s has already been stopped.", data_source)


def run_data_source_in_background(data_source, **kwargs):
    data_logger = logging.getLogger(__name__)

    override_param = override_parameters(data_source, **kwargs)

    if data_source_list[data_source]["process"] is not None and data_source_list[data_source]["process"].is_alive():
        if data_source_list[data_source]["run_parameters"] == {**override_param}:
            data_logger.info("Data source %s is already running.", data_source)
        else:
            data_logger.info("Restarting data source %s with new config.", data_source)
            stop_data_source(data_source)
            start_data_source_in_background(data_source, **override_param)
    else:
        start_data_source_in_background(data_source, **override_param)


def start_data_source_in_background(data_source, **kwargs):
    data_logger = logging.getLogger(__name__)

    data_logger.info(
        "Starting data source %s in background with config: %s",
        data_source,
        ", ".join("{0}={1!r}".format(k, v) for k, v in kwargs.items()),
    )
    data_source_list[data_source]["run_parameters"] = {**kwargs}

    data_logger.info("Saving updated config.")
    save_data_source_config()

    p = get_context("spawn").Process(target=run_data_source, args=(data_source,), kwargs={**kwargs})
    p.daemon = True
    p.start()
    data_logger.info("Now running data source %s in background.", data_source)
    data_source_list[data_source]["process"] = p


if __name__ == "__main__":
    logger = logging.getLogger(__name__)
    logger.error("This should not be run on it's own...")
