import logging
import multiprocessing
import sched

import gevent
from streamgraphicserver.data_sources.gg.smash.util.state import SmashGGState
import time

from gql import Client, gql
from gql.transport.requests import RequestsHTTPTransport
from streamgraphicserver.util.remote_data import send_data_multiple
from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.other import flatten

full_parsed = {"completed": [], "upcoming": [], "stream": []}
rotation_pos = {"completed": 0, "upcoming": 0, "stream": 0}
NUM_DISPLAY = {"completed": 2, "upcoming": 2, "stream": 1}


def parse_match_data(match, stream=False):
    global full_parsed

    logger = logging.getLogger(__name__)

    p1 = match["slots"][0]
    p2 = match["slots"][1]

    if (p1["entrant"] and p1["standing"]["stats"]["score"]["value"]) or (p2["entrant"] and p2["standing"]["stats"]["score"]["value"]):
        complete_score = True
    else:
        complete_score = False

    if p1["entrant"]:
        p1_name = p1["entrant"]["name"]
        p1_seed = str(p1["entrant"]["seeds"][0]["seedNum"])
        p1_seed_str = "[" + str(p1["entrant"]["seeds"][0]["seedNum"]) + "]"
        if complete_score:
            p1_score = str(int(p1["standing"]["stats"]["score"]["value"] or 0))
        else:
            p1_score = ""
    else:
        p1_name = ""
        p1_seed = ""
        p1_seed_str = ""
        p1_score = ""

    if p2["entrant"]:
        p2_name = p2["entrant"]["name"]
        p2_seed = str(p2["entrant"]["seeds"][0]["seedNum"])
        p2_seed_str = "[" + str(p2["entrant"]["seeds"][0]["seedNum"]) + "]"
        if complete_score:
            p2_score = str(int(p2["standing"]["stats"]["score"]["value"] or 0))
        else:
            p2_score = ""
    else:
        p2_name = ""
        p2_seed = ""
        p2_seed_str = ""
        p2_score = ""

    data = {
        "identifier": match["identifier"],
        "state": SmashGGState(match["state"]).name,
        "round_name": match["fullRoundText"],
        "p1": {
            "seed": p1_seed,
            "seed_str": p1_seed_str,
            "name": p1_name,
            "score": p1_score,
        },
        "p2": {
            "seed": p2_seed,
            "seed_str": p2_seed_str,
            "name": p2_name,
            "score": p2_score,
        },
    }

    logger.info(data)

    if stream:
        full_parsed["stream"].append(data)
    elif SmashGGState(match["state"]) == SmashGGState.COMPLETED:
        full_parsed["completed"].append(data)
    else:
        full_parsed["upcoming"].append(data)


query = gql(
    """
query StreamQueueOnTournament($tourneySlug: String!) {
  tournament(slug: $tourneySlug) {
    id
    streamQueue {
      stream {
        streamName
      }
      sets {
        fullRoundText
        state
        identifier
        slots {
          entrant {
            name
            seeds {
              seedNum
            }
          }
          standing {
            stats {
              score {
                value
              }
            }
          }
        }
      }
    }
    events(filter: {published: true}) {
      sets(filters: {showByes: false, hideEmpty: true}) {
        nodes{
          fullRoundText
          state
          identifier
          slots {
            entrant {
              name
              seeds {
                seedNum
              }
            }
            standing {
              stats {
                score {
                  value
                }
              }
            }
          }
        }
      }
    }
  }
}
"""
)


def update_data(scheduler, client, tournament_name):
    global full_parsed

    logger = logging.getLogger(__name__)

    try:
        logger.info("Requesting data.")

        result = client.execute(query, variable_values={"tourneySlug": tournament_name})

        full_parsed = {"completed": [], "upcoming": [], "stream": []}

        if result["tournament"]["streamQueue"]:
            for match in result["tournament"]["streamQueue"][0]["sets"]:
                parse_match_data(match, stream=True)

        if result["tournament"]["events"][0]["sets"]["nodes"]:
            for match in result["tournament"]["events"][0]["sets"]["nodes"]:
                parse_match_data(match)

        # send_data_multiple(flatten(info, parent_key='data/io/gitlab/osubgc/results'))
    except:
        logger.error("Failed to get smashgg data.")

    scheduler.enter(30, 1, update_data, argument=(scheduler, client, tournament_name))


def update_cycle(scheduler):
    global full_parsed
    global rotation_pos

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    for type in full_parsed.keys():
        total_show = len(full_parsed[type])
        if total_show > 1:
            for display_slot in range(0, NUM_DISPLAY[type]):
                rotation_pos[type] = rotation_pos[type] % total_show

                logger.info("Displaying %i/%i for %s", rotation_pos[type] + 1, total_show, type)

                send_data_multiple(
                    flatten(
                        full_parsed[type][rotation_pos[type]],
                        parent_key="data/gg/smash/smashgg/tournament/sets/cycle/" + type + "/" + str(display_slot),
                    )
                )
                # Move to the next result
                rotation_pos[type] = (rotation_pos[type] + 1) % total_show
        elif total_show > 0:
            rotation_pos[type] = rotation_pos[type] % total_show

            logger.info("Displaying %i/%i for %s", rotation_pos[type] + 1, total_show, type)

            send_data_multiple(
                flatten(
                    full_parsed[type][rotation_pos[type]],
                    parent_key="data/gg/smash/smashgg/tournament/sets/cycle/" + type + "/0",
                )
            )

            if NUM_DISPLAY[type] > 1:
                for display_slot in range(1, NUM_DISPLAY[type]):
                    logger.info(
                        "Blanking slot %i for %s as there is no enough data.",
                        display_slot,
                        type,
                    )

                    send_data_multiple(
                        flatten(
                            {
                                "identifier": "",
                                "state": "",
                                "round_name": "",
                                "p1": {
                                    "seed": "",
                                    "seed_str": "",
                                    "name": "",
                                    "score": "",
                                },
                                "p2": {
                                    "seed": "",
                                    "seed_str": "",
                                    "name": "",
                                    "score": "",
                                },
                            },
                            parent_key="data/gg/smash/smashgg/tournament/sets/cycle/" + type + "/" + str(display_slot),
                        )
                    )

            # Move to the next result
            rotation_pos[type] = (rotation_pos[type] + 1) % total_show
        else:
            logger.info("Displaying blank for %s as there is 0 data.", type)

            send_data_multiple(
                flatten(
                    {
                        "identifier": "",
                        "state": "",
                        "round_name": "",
                        "p1": {
                            "seed": "",
                            "seed_str": "",
                            "name": "",
                            "score": "",
                        },
                        "p2": {
                            "seed": "",
                            "seed_str": "",
                            "name": "",
                            "score": "",
                        },
                    },
                    parent_key="data/gg/smash/smashgg/tournament/sets/cycle/" + type + "/0",
                )
            )

    scheduler.enter(12.5, 1, update_cycle, argument=(scheduler,))


def run(smashgg_api_key, tournament_name):
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Starting gg/smash/tournament_stream data_source.")

    transport = RequestsHTTPTransport(
        url="https://api.smash.gg/gql/alpha",
        headers={"Authorization": "Bearer " + smashgg_api_key},
        verify=True,
        retries=3,
    )

    client = Client(transport=transport, fetch_schema_from_transport=True)

    scheduler = sched.scheduler(time.time, gevent.sleep)
    scheduler.enter(0, 1, update_data, argument=(scheduler, client, tournament_name))
    scheduler.enter(0, 1, update_cycle, argument=(scheduler,))
    scheduler.run()


if __name__ == "__main__":
    run()
