import logging
import multiprocessing
import sched

import gevent
from streamgraphicserver.data_sources.gg.smash.util.state import SmashGGState
import time

from gql import Client, gql
from gql.transport.requests import RequestsHTTPTransport
from streamgraphicserver.util.remote_data import send_data_multiple
from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.other import flatten

full_parsed = {"completed": [], "next": []}
rotation_pos = 0


def parse_match_data(match, completed=False):
    global full_parsed

    logger = logging.getLogger(__name__)

    p1 = match["slots"][0]
    p2 = match["slots"][1]

    if (p1["entrant"] and p1["standing"]["stats"]["score"]["value"]) or (p2["entrant"] and p2["standing"]["stats"]["score"]["value"]):
        complete_score = True
    else:
        complete_score = False

    if p1["entrant"]:
        p1_name = p1["entrant"]["name"]
        p1_seed = str(p1["seed"]["groupSeedNum"])
        p1_seed_str = "[" + str(p1["seed"]["groupSeedNum"]) + "]"
        if complete_score:
            p1_score = str(int(p1["standing"]["stats"]["score"]["value"] or 0))
        else:
            p1_score = ""
    else:
        p1_name = ""
        p1_seed = ""
        p1_seed_str = ""
        p1_score = ""

    if p2["entrant"]:
        p2_name = p2["entrant"]["name"]
        p2_seed = str(p2["seed"]["groupSeedNum"])
        p2_seed_str = "[" + str(p2["seed"]["groupSeedNum"]) + "]"
        if complete_score:
            p2_score = str(int(p2["standing"]["stats"]["score"]["value"] or 0))
        else:
            p2_score = ""
    else:
        p2_name = ""
        p2_seed = ""
        p2_seed_str = ""
        p2_score = ""

    data = {
        "identifier": match["phaseGroup"]["displayIdentifier"],
        "state": SmashGGState(match["state"]).name,
        "round_name": "Pool " + str(match["phaseGroup"]["displayIdentifier"]) + " - " + match["fullRoundText"],
        "time": match["completedAt"],
        "p1": {
            "seed": p1_seed,
            "seed_str": p1_seed_str,
            "name": p1_name,
            "score": p1_score,
        },
        "p2": {
            "seed": p2_seed,
            "seed_str": p2_seed_str,
            "name": p2_name,
            "score": p2_score,
        },
    }

    logger.info(data)

    if completed:
        full_parsed["completed"].append(data)
    else:
        full_parsed["next"].append(data)


query = gql(
    """
query PhaseGroupSets($phaseGroupId: ID!, $monitoredEntrants: [ID]!, $completedStates: [Int]!, $inProgressStates: [Int]!){
  completed: phase(id:$phaseGroupId){
    id
    sets(
      sortType: STANDARD
      filters: {
      	entrantIds: $monitoredEntrants
        state: $completedStates
      }
    ){
      pageInfo{
        total
      }
      nodes{
        fullRoundText
        completedAt
        phaseGroup {
          displayIdentifier
        }
        state
        slots{
          seed {
            groupSeedNum
          }
          entrant{
            name
          }
          standing {
            stats {
              score {
                value
              }
            }
          }
        }
      }
    }
  }
  next: phase(id:$phaseGroupId){
    id
    sets(
      sortType: STANDARD
      filters: {
      	entrantIds: $monitoredEntrants
        state: $inProgressStates
      }
    ){
      pageInfo{
        total
      }
      nodes{
        fullRoundText
        completedAt
        phaseGroup {
          displayIdentifier
        }
        state
        slots{
          seed {
            groupSeedNum
          }
          entrant{
            name
          }
          standing {
            stats {
              score {
                value
              }
            }
          }
        }
      }
    }
  }
}
"""
)


def update_data(scheduler, client, phaseGroupId, monitoredEntrants):
    global full_parsed

    logger = logging.getLogger(__name__)

    try:

        data_config = {
            "phaseGroupId": int(phaseGroupId),
            "monitoredEntrants": [int(x.strip()) for x in monitoredEntrants.split(",")],
            "completedStates": [3, 5],
            "inProgressStates": [1, 2, 4, 6, 7],
        }

        logger.info("Requesting data.", data_config)

        result = client.execute(
            query,
            variable_values=data_config,
        )

        full_parsed = {"completed": [], "next": []}

        if result["completed"]["sets"]["nodes"]:
            for match in result["completed"]["sets"]["nodes"]:
                parse_match_data(match, completed=True)

        if result["next"]["sets"]["nodes"]:
            for match in result["next"]["sets"]["nodes"]:
                parse_match_data(match, completed=False)

        send_data_multiple(
            flatten(
                full_parsed["next"],
                parent_key="data/gg/smash/smashgg/monitored_phase/sets/cycle/next",
            )
        )

        for i in range(4, len(full_parsed["next"]), -1):
            i = i - 1

            logger.info(
                "Displaying blank for next " + str(i) + " as there is 0 data.",
            )

            send_data_multiple(
                flatten(
                    {
                        "identifier": "",
                        "state": "",
                        "round_name": "",
                        "p1": {
                            "seed": "",
                            "seed_str": "",
                            "name": "",
                            "score": "",
                        },
                        "p2": {
                            "seed": "",
                            "seed_str": "",
                            "name": "",
                            "score": "",
                        },
                    },
                    parent_key="data/gg/smash/smashgg/monitored_phase/sets/cycle/next/" + str(i),
                )
            )
    except:
        logger.error("Failed to get smashgg data.", exc_info=True)

    scheduler.enter(
        15,
        1,
        update_data,
        argument=(scheduler, client, phaseGroupId, monitoredEntrants),
    )


def update_cycle(scheduler):
    global full_parsed
    global rotation_pos

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    total_show = len(full_parsed["completed"])
    if total_show > 1:

        rotation_pos = rotation_pos % total_show

        logger.info("Displaying %i/%i", rotation_pos + 1, total_show)

        # send_data_multiple(
        #    flatten(
        #        full_parsed["completed"][rotation_pos],
        #        parent_key="data/gg/smash/smashgg/monitored_phase/sets/cycle/completed/0",
        #    )
        # )
        # Move to the next result
        rotation_pos = (rotation_pos + 1) % total_show
    else:
        logger.info(
            "Displaying blank for completed as there is 0 data.",
        )

        send_data_multiple(
            flatten(
                {
                    "identifier": "",
                    "state": "",
                    "round_name": "",
                    "p1": {
                        "seed": "",
                        "seed_str": "",
                        "name": "",
                        "score": "",
                    },
                    "p2": {
                        "seed": "",
                        "seed_str": "",
                        "name": "",
                        "score": "",
                    },
                },
                parent_key="data/gg/smash/smashgg/monitored_phase/sets/cycle/completed_rot/0",
            )
        )
    send_data_multiple(
        flatten(
            sorted(full_parsed["completed"], key=lambda t: t["time"], reverse=True),
            parent_key="data/gg/smash/smashgg/monitored_phase/sets/cycle/completed",
        )
    )

    scheduler.enter(10, 1, update_cycle, argument=(scheduler,))


def run(smashgg_api_key, phaseGroupId, monitoredEntrants):
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Starting gg/smash/tournament_stream data_source.")

    transport = RequestsHTTPTransport(
        url="https://api.smash.gg/gql/alpha",
        headers={"Authorization": "Bearer " + smashgg_api_key},
        verify=True,
        retries=3,
    )

    client = Client(transport=transport, fetch_schema_from_transport=True)

    scheduler = sched.scheduler(time.time, gevent.sleep)
    scheduler.enter(0, 1, update_data, argument=(scheduler, client, phaseGroupId, monitoredEntrants))
    scheduler.enter(0, 1, update_cycle, argument=(scheduler,))
    scheduler.run()


if __name__ == "__main__":
    run()
