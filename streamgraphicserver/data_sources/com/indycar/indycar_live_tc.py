import json
import logging
import multiprocessing
import sched
import time

import gevent
import requests
from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.other import flatten
from streamgraphicserver.util.remote_data import send_data_multiple


setup_loggers()

multiprocessing_logger = multiprocessing.get_logger()
multiprocessing_logger.setLevel(logging.INFO)
logger = logging.getLogger(__name__)

BUFFER_MAX = 6

driver_info = {}
buffer_window = []


def update_data(scheduler=None):
    try:
        session_tc_raw = requests.get("https://indycarsso.blob.core.windows.net/racecontrol/timingscoring.json").text

        session_tc = json.loads(session_tc_raw.replace("jsonCallback(", "").replace(");", ""))

        extra_info = []
        by_rank = {}
        by_rank_extra = {}

        session_tc["timing_results"]["heartbeat"]["lapNumberDisplay"] = int(session_tc["timing_results"]["heartbeat"]["lapNumber"]) + 1

        for tc_row in session_tc["timing_results"]["Item"]:
            extra_info.append(driver_info[tc_row["DriverID"]])
            by_rank[tc_row["rank"]] = tc_row
            by_rank_extra[tc_row["rank"]] = driver_info[tc_row["DriverID"]]

        buffer_window.append(
            {
                "tc": session_tc,
                "tc_extra": session_tc,
                "tc_rank": by_rank,
                "tc_rank_extra": by_rank_extra,
            }
        )

        if len(buffer_window) == BUFFER_MAX:
            send_data_multiple(flatten(buffer_window.pop(0), parent_key="data/com/indycar"))
        else:
            logger.info("Building buffer: " + str(len(buffer_window)) + "/" + str(BUFFER_MAX))
            print("com/indycar/tc - Building buffer: " + str(len(buffer_window)) + "/" + str(BUFFER_MAX))
    except requests.exceptions.ConnectionError:
        pass

    scheduler.enter(5, 1, update_data, argument=(scheduler,))


def run():
    global driver_info

    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Running com/indycar/tc data_source:")

    logger.info("Getting driver info...")

    drivers_raw = requests.get("https://feeds.indycar.com/driversfeed.json").text
    drivers = json.loads(drivers_raw.replace("driverCallback(", "").replace(");", ""))["drivers"]["driver"]

    driver_by_num = {}

    for driver in drivers:
        driver_info[driver["rc_driver_id"]] = driver
        driver_by_num[driver["number"]] = driver

    send_data_multiple(flatten(driver_by_num, parent_key="data/com/indycar/drivers"))

    logger.info("Driver info gathered.")

    s = sched.scheduler(time.time, gevent.sleep)

    s.enter(0, 1, update_data, argument=(s,))
    s.run()


if __name__ == "__main__":
    s = sched.scheduler(time.time, gevent.sleep)

    s.enter(0, 1, update_data, argument=(s,))
    s.run()
