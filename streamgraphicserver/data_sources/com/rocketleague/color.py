import websockets
import asyncio
import logging
import multiprocessing

from streamgraphicserver.util.logging import setup_loggers


async def send_colors(
    bakkesmod_server="localhost",
    team0_primary="#b3c8e3",
    team0_secondary="#49586b",
    team1_primary="#bb0000",
    team1_secondary="#bb0000",
    rcon_password="password",
):
    async with websockets.connect("ws://" + bakkesmod_server + ":9002", timeout=0.1) as websocket:
        await websocket.send("rcon_password " + rcon_password)
        auth_status = await websocket.recv()
        assert auth_status == "authyes"

        await websocket.send("replay_gui hud 0")
        await websocket.send("replay_gui matchinfo 0")
        await websocket.send("replay_gui names 1")
        print("Sent 2")
        await websocket.send("Colors_Preference_Blue_Primary " + team0_primary)
        await websocket.send("Colors_Preference_Blue_Secondary " + team0_secondary)
        await websocket.send("Colors_Preference_Orange_Primary " + team1_primary)
        await websocket.send("Colors_Preference_Orange_Secondary " + team1_secondary)
        print("Sent 3")


def run(
    bakkesmod_server="localhost",
    team0_primary="#b3c8e3",
    team0_secondary="#49586b",
    team1_primary="#bb0000",
    team1_secondary="#bb0000",
    rcon_password="password",
):
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info(
        "Starting com/rocketleague/rl_color data_source: bakkesmod_server=%s, team0_primary=%s, team0_secondary=%s, team1_primary=%s, team1_secondary=%s",
        bakkesmod_server,
        team0_primary,
        team0_secondary,
        team1_primary,
        team1_secondary,
    )
    asyncio.get_event_loop().run_until_complete(
        send_colors(
            bakkesmod_server,
            team0_primary,
            team0_secondary,
            team1_primary,
            team1_secondary,
            rcon_password,
        )
    )
    print("colors set - stopped")
    exit()


if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(send_colors())
