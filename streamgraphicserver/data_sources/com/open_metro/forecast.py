import logging
import multiprocessing
import sched
import time
import traceback
import urllib.parse

import gevent
import requests
from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.other import flatten
from streamgraphicserver.util.remote_data import send_data_multiple

test_mode = True

WMO_CODE_TEXT = {
    0: "Clear sky",
    1: "Mainly clear",
    2: "Partly cloudy",
    3: "Overcast",
    45: "Fog",
    48: "Depositing rime fog",
    51: "Drizzle: Light",
    53: "Drizzle: Moderate",
    55: "Drizzle: Dense",
    56: "Freezing Drizzle: Light",
    57: "Freezing Drizzle: Dense",
    61: "Rain: Slight",
    63: "Rain: Moderate",
    65: "Rain: Heavy",
    66: "Freezing Rain: Light",
    67: "Freezing Rain: Heavy",
    71: "Snow fall: Slight",
    73: "Snow fall: Moderate",
    75: "Snow fall: Heavy",
    77: "Snow grains",
    80: "Rain showers: Slight",
    81: "Rain showers: Moderate",
    82: "Rain showers: Heavy",
    85: "Snow showers Slight",
    86: "Snow showers Heavy",
    95: "Thunderstorm",
    96: "Thunderstorm: Slight Hail",
    99: "Thunderstorm: Heavy Hail",
}


def update_weather(lat, long, location_name=None, scheduler=None):
    logger = logging.getLogger(__name__)

    if not location_name:
        location_name = str(lat) + "," + str(long)

    params = {
        "latitude": str(lat),
        "longitude": str(long),
        "hourly": "temperature_2m,relativehumidity_2m,apparent_temperature,precipitation,weathercode,snow_depth,cloudcover",
        "daily": "weathercode,temperature_2m_max,temperature_2m_min,apparent_temperature_max,apparent_temperature_min,precipitation_sum,precipitation_hours,windspeed_10m_max,windgusts_10m_max,winddirection_10m_dominant",
        "current_weather": "true",
        "temperature_unit": "fahrenheit",
        "windspeed_unit": "mph",
        "precipitation_unit": "inch",
        "timezone": "America/New_York",
    }

    params_str = urllib.parse.urlencode(params, safe=",")

    try:
        wthr_data = requests.get(
            "https://api.open-meteo.com/v1/forecast",
            params=params_str,
            timeout=(3.05, 27),
        ).json()

        wthr_data["hourly_chart"] = {}

        for wthr_hour_key in wthr_data["hourly"].keys():
            wthr_data["hourly_chart"][wthr_hour_key] = ",".join(map(str, wthr_data["hourly"][wthr_hour_key]))

        wthr_data["current_weather"]["text"] = WMO_CODE_TEXT[wthr_data["current_weather"]["weathercode"]]
        wthr_data["current_weather"]["weathercode"] = int(wthr_data["current_weather"]["weathercode"])
        wthr_data["daily"]["text"] = {}
        wthr_data["hourly"]["text"] = {}

        for wthr_daily_key, wthr_code in enumerate(wthr_data["daily"]["weathercode"]):
            wthr_data["daily"]["weathercode"][wthr_daily_key] = int(wthr_code)
            wthr_data["daily"]["text"][wthr_daily_key] = WMO_CODE_TEXT[int(wthr_code)]

        for wthr_hourly_key, wthr_code in enumerate(wthr_data["hourly"]["weathercode"]):
            wthr_data["hourly"]["weathercode"][wthr_daily_key] = int(wthr_code)
            wthr_data["hourly"]["text"][wthr_hourly_key] = WMO_CODE_TEXT[int(wthr_code)]

        send_data_multiple(
            flatten(
                wthr_data,
                parent_key="data/com/open-metro/forecast/" + location_name,
            )
        )
    except:
        traceback.print_exc()

    if scheduler:
        scheduler.enter(120, 1, update_weather, argument=(lat, long, location_name, scheduler))


def run(lat=39.999436, long=-83.012724):
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Starting com/open-metro/forecast data_source: lat=%s, long=%s", lat, long)
    scheduler = sched.scheduler(time.time, gevent.sleep)

    scheduler.enter(0, 1, update_weather, argument=(lat, long, None, scheduler))
    scheduler.run()
