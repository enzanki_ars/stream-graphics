from datetime import datetime
import logging
import multiprocessing
import sched
import time
import gevent

import requests
from streamgraphicserver.util.data import delete_keys

from streamgraphicserver.util.remote_data import send_data_multiple
from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.other import flatten
from streamgraphicserver.util.config import get_config


def update_data(scheduler=None, group=""):
    logger = logging.getLogger(__name__)

    delete_keys(search="data/com/ncaa/mm/*")

    group = str(group)

    session = requests.Session()
    session.trust_env = False

    logger.info("Requesting bracket update.")
    group_json = session.get(
        "https://play.ncaa.com/mens-bracket-challenge/api/static-v2/ncaabracketchallenge/leaderboard/group/" + group + "/0.json",
        timeout=(3.05, 27),
    ).json()["entries"]

    entry_data = {}
    entry_picks = {}

    for entry in group_json:
        entry["color"] = get_config().get("config/com/ncaa/mm/bracket/entry/" + entry["id"] + "/color", "#000000")
        entry["style/background-color"] = entry["color"]
        entry["icon"] = get_config().get("config/com/ncaa/mm/bracket/entry/" + entry["id"] + "/icon", "")

        entry_data[entry["name"]] = entry
        entry_picks[entry["name"]] = {}

        entry_json = session.get(
            "https://play.ncaa.com/mens-bracket-challenge/api/static-v2/ncaabracketchallenge/entries/" + entry["id"] + "/show.json",
            timeout=(3.05, 27),
        ).json()["entry"]

        entry["overall-rank"] = entry_json["rank"]
        entry["overall-percentile"] = entry_json["percentile"]

        for pick in entry_json["picks"]:
            entry_picks[entry["name"]][int(pick["gameId"])] = int(pick["teamId"])

    progress_matches = []
    upcoming_matches = []
    complete_matches = []

    logger.info("Requesting matches update.")

    matches_json = session.get(
        "https://sdataprod.ncaa.com/?operationName=scores_bracket_web&variables=%7B%22seasonYear%22%3A2022%2C%22current%22%3Atrue%7D&extensions=%7B%22persistedQuery%22%3A%7B%22version%22%3A1%2C%22sha256Hash%22%3A%22f21cac8420a55a7d190f2f686a441e2507d8fb80f25eac5c91131ddd9df588da%22%7D%7D",
        timeout=(3.05, 27),
    ).json()["data"]["mmlContests"]

    for match in filter(lambda m: m["bracketId"] > 200 and len(m["teams"]) == 2, sorted(matches_json, key=lambda m: m["startTimeEpoch"])):
        del match["location"]
        del match["mmlStreams"]
        del match["excitementAlerts"]

        for team in match["teams"]:
            team["icon-dark"] = "https://i.turner.ncaa.com/sites/default/files/images/logos/schools/bgd/" + team["seoname"] + ".svg"
            team["icon"] = "https://i.turner.ncaa.com/sites/default/files/images/logos/schools/bgl/" + team["seoname"] + ".svg"
            team["style/background-color"] = team["color"]

        home_team = match["teams"][0]["ncaaOrgId"]
        home_team_icon = match["teams"][0]["icon"]
        home_team_score = match["teams"][0]["score"]
        away_team = match["teams"][1]["ncaaOrgId"]
        away_team_icon = match["teams"][1]["icon"]
        away_team_score = match["teams"][1]["score"]

        match["picks"] = {}

        for entry, picks in entry_picks.items():
            picked_team = picks[match["bracketId"]]

            if picked_team == home_team:
                picked_team_icon = home_team_icon
            elif picked_team == away_team:
                picked_team_icon = away_team_icon
            else:
                picked_team_icon = ""

            prediction_state = "#FFFFFF"

            if home_team_score and away_team_score:
                if home_team_score > away_team_score:
                    if picked_team == home_team:
                        prediction_state = "#CCFFCC"
                    elif picked_team == away_team:
                        prediction_state = "#FFCCCC"
                elif home_team_score < away_team_score:
                    if picked_team == home_team:
                        prediction_state = "#FFCCCC"
                    elif picked_team == away_team:
                        prediction_state = "#CCFFCC"
                else:
                    prediction_state = "#CCCCFF"

            match["picks"][entry] = {
                "picked_team_icon": picked_team_icon,
                "prediction_state": prediction_state,
                "prediction_state/style/background-color": prediction_state,
            }

        if match["gameState"] == "P":
            match["short"] = datetime.fromtimestamp(int(match["startTimeEpoch"])).strftime("%-m/%d %-I:%M %p")
            match["short-line1"] = datetime.fromtimestamp(int(match["startTimeEpoch"])).strftime("%-m/%d")
            match["short-line2"] = datetime.fromtimestamp(int(match["startTimeEpoch"])).strftime("%-I:%M %p")
            upcoming_matches.append(match)
        elif match["gameState"] == "I":
            match["short"] = match["currentPeriod"] + " " + match["contestClock"]
            match["short-line1"] = match["currentPeriod"].replace("HALFTIME", "HALF")
            match["short-line2"] = match["contestClock"]
            progress_matches.append(match)
        elif match["gameState"] == "F":
            match["short"] = match["round"]["title"]
            match["short-line1"] = match["round"]["title"].split(" ")[0]
            match["short-line2"] = match["round"]["title"].split(" ")[0]
            complete_matches.append(match)
        else:
            print("UNKNOWN GAMESTATE", match["gameStateCode"])

    logger.info("Updating data.")

    send_data_multiple(flatten(group_json, parent_key="data/com/ncaa/mm/group/" + group + "/scoreboard"))
    send_data_multiple(flatten(entry_data, parent_key="data/com/ncaa/mm/group/" + group + "/entries"))

    displaying_matches = progress_matches + upcoming_matches[0:8]
    displaying_matches += list(reversed(complete_matches))[0 : (14 - len(displaying_matches))]

    send_data_multiple(flatten(displaying_matches, parent_key="data/com/ncaa/mm/matches"))

    scheduler.enter(30, 1, update_data, argument=(scheduler, group))


def run(group=""):
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Starting com/ncaa/mm data_source: group=%s", group)
    scheduler = sched.scheduler(time.time, gevent.sleep)

    scheduler.enter(0, 1, update_data, argument=(scheduler, group))
    scheduler.run()
