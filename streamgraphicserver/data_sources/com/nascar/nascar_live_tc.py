import copy
import logging
import struct
import traceback

import requests
import websocket
from streamgraphicserver.util.other import flatten
from streamgraphicserver.util.remote_data import send_data_multiple

LOGGER = logging.getLogger(__name__)
LIVE_FEED_REFRESH_PACKETS = 30

MSGS_SINCE_FEED = 99
TELEMETRY_CARID = {}
LIVE_FEED = {}


def on_open(wsapp):
    print("CONNECTED")


def on_error(wsapp, error):
    print("ERROR", error)


def padded_binary_string(data, start, length=1):
    return "{0:b}".format(struct.unpack("B", data[start : start + length])[0]).rjust(8, "0")


def get_live_feed():
    global TELEMETRY_CARID
    global LIVE_FEED

    print("Refreshing live feed.")
    curr_feed = requests.get("https://cf.nascar.com/cacher/live/live-feed.json").json()

    LIVE_FEED = copy.deepcopy(curr_feed)

    LIVE_FEED["vehicles"] = {}
    LIVE_FEED["position"] = {}

    for vehicle in curr_feed["vehicles"]:
        vehicle["pit_stops_num"] = len(vehicle["pit_stops"])
        vehicle["pit_stops_last"] = vehicle["pit_stops"][-1]
        vehicle["pit_stops_last"]["pit_time"] = vehicle["pit_stops_last"]["pit_out_elapsed_time"] - vehicle["pit_stops_last"]["pit_in_elapsed_time"]
        vehicle["pit_stops_since"] = LIVE_FEED["lap_number"] - vehicle["pit_stops"][-1]["pit_in_leader_lap"]

        vehicle["laps_led_num"] = 0

        for laps_led_data in vehicle["laps_led"]:
            vehicle["laps_led_num"] += laps_led_data["end_lap"] - laps_led_data["start_lap"]

        del vehicle["laps_led"]
        del vehicle["pit_stops"]

        if vehicle["vehicle_number"] in TELEMETRY_CARID.keys():
            curr_telemetry = TELEMETRY_CARID[vehicle["vehicle_number"]]
        else:
            curr_telemetry = {}

        LIVE_FEED["position"][vehicle["running_position"]] = {**vehicle, **curr_telemetry}
        LIVE_FEED["vehicles"][vehicle["vehicle_number"]] = {**vehicle, **curr_telemetry}


def on_message(wsapp, message):
    global TELEMETRY_CARID
    global MSGS_SINCE_FEED
    # print(message)
    # print(type(message[6]))

    try:
        if struct.unpack("c", message[6:7])[0] == "s".encode("utf8"):
            print("TELEMETRY")
            msg_body = message[7:]

            b7 = padded_binary_string(msg_body, 7)
            b8 = padded_binary_string(msg_body, 8)

            str_num_cars = b7[4:8] + b8[0:2]
            num_cars = int(str_num_cars, 2)

            print("\t", num_cars, "cars")
            vehicles = message[16:]

            for car_data_pos in range(0, num_cars):
                data_start = car_data_pos * 15

                cD0 = padded_binary_string(vehicles, data_start + 0)

                cD4 = padded_binary_string(vehicles, data_start + 4)
                cD5 = padded_binary_string(vehicles, data_start + 5)
                cD6 = padded_binary_string(vehicles, data_start + 6)
                cD7 = padded_binary_string(vehicles, data_start + 7)
                cD8 = padded_binary_string(vehicles, data_start + 8)
                cD9 = padded_binary_string(vehicles, data_start + 9)
                cD10 = padded_binary_string(vehicles, data_start + 10)

                speed = int(cD4[3:8] + cD5 + cD6[0:5], 2) / 1000
                throttle = int(cD6[5:8] + cD7[0:4], 2)
                brake = int(cD7[4:8] + cD8[0:3], 2)
                rpm = int(cD8[3:8] + cD9[0:7], 2) * 4
                fuel = int(cD9[7:8] + cD10[0:6], 2)

                carId = int(cD0, 2) / 2

                if not int(cD0, 2) % 2 == 0:
                    carId = "0" + str((carId * 2 - 1) / 2)

                carId = str(carId).split(".")[0]

                TELEMETRY_CARID[carId] = {
                    "live_pos": car_data_pos + 1,
                    "live_carId": carId,
                    "live_speed": speed,
                    "live_rpm": rpm,
                    "live_fuel": fuel,
                    "live_brake": brake,
                    "live_throttle": throttle,
                }

        if MSGS_SINCE_FEED > LIVE_FEED_REFRESH_PACKETS:
            MSGS_SINCE_FEED = 0
            get_live_feed()
        else:
            MSGS_SINCE_FEED += 1

            prev_feed_v = copy.deepcopy(LIVE_FEED["vehicles"])

            LIVE_FEED["vehicles"] = {}
            LIVE_FEED["position"] = {}

            for vehicle_num, vehicle in prev_feed_v.items():
                if vehicle_num not in TELEMETRY_CARID.keys():
                    print(vehicle_num, "not in", TELEMETRY_CARID.keys())

                curr_telemetry = TELEMETRY_CARID[vehicle_num]

                LIVE_FEED["position"][curr_telemetry["live_pos"]] = {**vehicle, **curr_telemetry}
                LIVE_FEED["vehicles"][curr_telemetry["live_carId"]] = {**vehicle, **curr_telemetry}

        print(LIVE_FEED["position"].keys())

        send_data_multiple(flatten(LIVE_FEED, parent_key="data/com/nascar/tc"))
    except:
        traceback.print_exc()


def run():
    LOGGER.info("Running com/nascar/tc data_source:")
    wsapp = websocket.WebSocketApp("wss://test.nascardatalive.com:443/raceviewweb", on_message=on_message, on_open=on_open, on_error=on_error)
    wsapp.run_forever()


if __name__ == "__main__":
    run()
