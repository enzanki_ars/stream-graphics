import json
import logging
import multiprocessing
import sched
import time
import gevent

import requests
from streamgraphicserver.util.remote_data import send_data_multiple
from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.other import flatten

REQUEST_SESSION = requests.Session()

COINBASE_API_PRO = "https://api.pro.coinbase.com/"
COINBASE_API_PRO_HISTORICAL = "products/{product}/candles?granularity=300"
COINBASE_API_PRO_TICKER = "products/{product}/ticker"

TRACKING_EXCHANGE = ["BTC-USD", "ETH-USD", "LTC-USD"]
GRAPH_SMOOTHING = 4


def update_data(scheduler=None):

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    data = {}

    for currency_exchange in TRACKING_EXCHANGE:
        coinbase_candles = REQUEST_SESSION.get(COINBASE_API_PRO + COINBASE_API_PRO_HISTORICAL.format(product=currency_exchange)).json()

        data[currency_exchange] = {
            "historical_1m": {
                "time": "",
                "low": "",
                "high": "",
                "open": "",
                "close": "",
                "volume": "",
                "change": "",
            }
        }

        first = True
        starting_open = 0
        final_change = 0

        for candle_data in sorted(coinbase_candles[::GRAPH_SMOOTHING], key=lambda x: x[0]):
            if first:
                data[currency_exchange]["historical_1m"]["time"] += str(candle_data[0])
                data[currency_exchange]["historical_1m"]["low"] += str(candle_data[1])
                data[currency_exchange]["historical_1m"]["high"] += str(candle_data[2])
                data[currency_exchange]["historical_1m"]["open"] += str(candle_data[3])
                data[currency_exchange]["historical_1m"]["close"] += str(candle_data[4])
                data[currency_exchange]["historical_1m"]["volume"] += str(candle_data[5])

                starting_open = candle_data[3]

                data[currency_exchange]["historical_1m"]["change"] += str((candle_data[4] - starting_open) / starting_open)

                first = False
            else:
                data[currency_exchange]["historical_1m"]["time"] += "," + str(candle_data[0])
                data[currency_exchange]["historical_1m"]["low"] += "," + str(candle_data[1])
                data[currency_exchange]["historical_1m"]["high"] += "," + str(candle_data[2])
                data[currency_exchange]["historical_1m"]["open"] += "," + str(candle_data[3])
                data[currency_exchange]["historical_1m"]["close"] += "," + str(candle_data[4])
                data[currency_exchange]["historical_1m"]["volume"] += "," + str(candle_data[5])

                data[currency_exchange]["historical_1m"]["change"] += "," + str((candle_data[4] - starting_open) / starting_open)
                final_change = (candle_data[4] - starting_open) / starting_open

        coinbase_ticker = REQUEST_SESSION.get(COINBASE_API_PRO + COINBASE_API_PRO_TICKER.format(product=currency_exchange)).json()

        data[currency_exchange]["historical_1m_change_calc"] = final_change * 100
        data[currency_exchange]["ticker"] = coinbase_ticker
        data[currency_exchange]["ticker"]["product_id"] = currency_exchange

    logger.info("Coinbase update.")

    send_data_multiple(flatten(data, parent_key="data/com/coinbase/pro/market"))

    scheduler.enter(60, 1, update_data, argument=(scheduler,))


def run():
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Running com/coinbase/coinbase_pro_market data_source:")

    s = sched.scheduler(time.time, gevent.sleep)

    s.enter(0, 1, update_data, argument=(s,))
    s.run()


if __name__ == "__main__":
    s = sched.scheduler(time.time, gevent.sleep)

    s.enter(0, 1, update_data, argument=(s,))
    s.run()
