import logging
import multiprocessing

import sched
import time
import json
import gevent

import requests

from streamgraphicserver.util.remote_data import delete_keys, send_data_multiple
from streamgraphicserver.util.other import flatten
from streamgraphicserver.util.logging import setup_loggers

logger = logging.getLogger(__name__)


def update_data(scheduler=None):
    try:
        session_info_raw = requests.get("https://scoring.imsa.com/scoring_data/SessionInfo_JSONP.json").text
        race_results_raw = requests.get("https://scoring.imsa.com/scoring_data/RaceResults_JSONP.json").text

        session_info = json.loads(session_info_raw.replace("jsonpSessionInfo(", "").replace(");", ""))

        race_results = json.loads(race_results_raw.replace("jsonpRaceResults(", "").replace(");", ""))

        delete_keys(search="data/com/imsa/tc/*")

        race_results["per_class"] = {
            "DPi": {},
            "LMP2": {},
            "LMP3": {},
            "GTDPRO": {},
            "GTLM": {},
            "GTD": {},
        }

        for driver in race_results["B"]:
            driver["last_name"] = driver["F"].split(" ")[-1]

            if driver["C"] in race_results["per_class"].keys():
                race_results["per_class"][driver["C"]][driver["PIC"]] = driver

        send_data_multiple(flatten(session_info, parent_key="data/com/imsa/tc/session_info"))
        send_data_multiple(flatten(race_results, parent_key="data/com/imsa/tc/race_results"))
    except requests.exceptions.ConnectionError:
        pass

    scheduler.enter(5, 1, update_data, argument=(scheduler,))


def run():
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Running com/imsa/tc data_source:")

    s = sched.scheduler(time.time, gevent.sleep)

    s.enter(0, 1, update_data, argument=(s,))
    s.run()


if __name__ == "__main__":
    s = sched.scheduler(time.time, gevent.sleep)

    s.enter(0, 1, update_data, argument=(s,))
    s.run()
