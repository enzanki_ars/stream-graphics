import logging
import multiprocessing

import twitter
from dateutil.parser import parse
from dateutil.tz import gettz
from pprint import pprint

from streamgraphicserver.util.config import get_config
from streamgraphicserver.util.remote_data import send_data, send_data_multiple
from streamgraphicserver.util.other import flatten
from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.restore import run_script_no_thread

logger = logging.getLogger(__name__)


def get_tweet(tweet_id, consumer_key, consumer_secret, access_token_key, access_token_secret):
    config = {
        "consumer_key": consumer_key,
        "consumer_secret": consumer_secret,
        "access_token_key": access_token_key,
        "access_token_secret": access_token_secret,
    }

    api = twitter.Api(**config, tweet_mode="extended")

    tweet = api.GetStatus(tweet_id)

    logger.info(tweet)

    print(tweet)

    tweet_text = tweet.full_text

    for url in tweet.urls:
        tweet_text = tweet_text.replace(url.url, "<a href='" + url.url + "'>" + url.expanded_url + "</a>")

    for hashtag in tweet.hashtags:
        tweet_text = tweet_text.replace("#" + hashtag.text, "<a href='https://twitter.com/hashtag/" + hashtag.text + "'>#" + hashtag.text + "</a>")
    for user_mention in tweet.user_mentions:
        tweet_text = tweet_text.replace(
            "@" + user_mention.screen_name, "<a href='https://twitter.com/" + user_mention.screen_name + "'>@" + user_mention.screen_name + "</a>"
        )

    tweet_text = tweet_text.replace("\n", "<br/>")

    data = {
        "avatar": tweet.user.profile_image_url_https.replace("_normal", "_bigger"),
        "name": tweet.user.name,
        "username": "@" + tweet.user.screen_name,
        "text": tweet_text,
        "timestamp": parse(tweet.created_at).astimezone(gettz("America/New_York")).strftime("%A, %d %B %Y, %I:%M%p"),
        "images": {"1": "", "2": "", "3": "", "4": ""},
        "video": "",
    }

    has_video = False

    image_count = 0

    if tweet.media is not None:
        for i, image in enumerate(tweet.media):
            image_count = i + 1
            data["images"][str(image_count)] = tweet.media[i].media_url_https

            data["text"] = data["text"].replace(tweet.media[i].url, "")

            if tweet.media[i].type == "video" or tweet.media[i].type == "animated_gif":
                has_video = True

                pprint(tweet.media[i].video_info["variants"])

                largest_video = max(
                    tweet.media[i].video_info["variants"],
                    key=lambda x: x.get("bitrate", 0),
                )

                data["video"] = largest_video["url"]

    pprint(data)

    send_data_multiple(flatten(data, parent_key="data/com/twitter/tweet"))

    if has_video:
        run_script_no_thread("restore/com/twitter/overlays/twitter", "video")
    else:
        run_script_no_thread("restore/com/twitter/overlays/twitter", "image-count-" + str(image_count))


def run(
    tweet=None,
    consumer_key=None,
    consumer_secret=None,
    access_token_key=None,
    access_token_secret=None,
):
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Retriving com/twitter/tweet data_source: tweet=%s", tweet)

    if tweet:
        get_tweet(
            int(tweet),
            consumer_key,
            consumer_secret,
            access_token_key,
            access_token_secret,
        )

    exit(0)


if __name__ == "__main__":
    get_tweet(1117187592850759680)
