import logging
import multiprocessing
import sched
import time
import gevent

import requests
from streamgraphicserver.util.remote_data import send_data_multiple
from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.other import flatten

logger = logging.getLogger(__name__)

GRAPH_SMOOTHING = 4
STOCKS = [
    {"name": "AMZN", "type": "stocks"},
    {"name": "AAPL", "type": "stocks"},
    # {"name": "AVGO", "type": "stocks"},
    # {"name": "COIN", "type": "stocks"},
    {"name": "DIS", "type": "stocks"},
    {"name": "META", "type": "stocks"},
    # {"name": "GME", "type": "stocks"},
    {"name": "GOOG", "type": "stocks"},
    # {"name": "GOOGL", "type": "stocks"},
    # {"name": "GPRO", "type": "stocks"},
    {"name": "INTC", "type": "stocks"},
    {"name": "MSFT", "type": "stocks"},
    {"name": "NFLX", "type": "stocks"},
    {"name": "NVDA", "type": "stocks"},
    # {"name": "PYPL", "type": "stocks"},
    {"name": "QCOM", "type": "stocks"},
    # {"name": "SQ", "type": "stocks"},
    {"name": "TSLA", "type": "stocks"},
    # {"name": "TWTR", "type": "stocks"},
    # {"name": "VZ", "type": "stocks"},
    {"name": "COMP", "type": "index"},
    {"name": "INDU", "type": "index"},
    {"name": "NDX", "type": "index"},
    {"name": "NYA", "type": "index"},
    # {"name": "RUT", "type": "index"},
    {"name": "SPX", "type": "index"},
]

HEADERS = {"user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36"}


def update_data(scheduler=None):
    logger = logging.getLogger(__name__)

    logger.info("Updating com/nasdaq/market")

    try:
        market_data = requests.get(
            "https://api.nasdaq.com/api/market-info",
            timeout=(3.05, 27),
            headers=HEADERS,
        ).json()

        send_data_multiple(flatten(market_data, parent_key="data/com/nasdaq/market/info"))

        for stock_info in STOCKS:
            stock_name = stock_info["name"]
            stock_type = stock_info["type"]

            stock_data = requests.get(
                "https://api.nasdaq.com/api/quote/" + stock_name + "/info?assetclass=" + stock_type,
                timeout=(3.05, 27),
                headers=HEADERS,
            ).json()

            stock_chart = requests.get(
                "https://api.nasdaq.com/api/quote/" + stock_name + "/chart?assetClass=" + stock_type,
                timeout=(3.05, 27),
                headers=HEADERS,
            ).json()

            chart_parsed = {
                "x": [],
                "y": [],
            }

            start_time = stock_chart["data"]["chart"][0]["x"]
            previous_close = float(stock_chart["data"]["previousClose"].replace("$", "").replace(",", ""))

            chart_data_process = stock_chart["data"]["chart"][::GRAPH_SMOOTHING]

            for data_point in sorted(chart_data_process, key=lambda d: d["x"]):
                chart_parsed["x"].append(int((data_point["x"] - start_time) / 1000))
                chart_parsed["y"].append("{0:.2f}".format(data_point["y"] - previous_close))

            chart_parsed_str = {
                "x": ",".join(map(str, chart_parsed["x"])),
                "y": ",".join(map(str, chart_parsed["y"])),
            }

            del stock_chart["data"]["chart"]
            if "volumeChart" in stock_chart["data"]:
                del stock_chart["data"]["volumeChart"]

            send_data_multiple(
                flatten(
                    stock_data,
                    parent_key="data/com/nasdaq/market/stock/" + stock_name + "/quote",
                )
            )
            send_data_multiple(
                flatten(
                    stock_chart,
                    parent_key="data/com/nasdaq/market/stock/" + stock_name + "/chart",
                )
            )
            send_data_multiple(
                flatten(
                    chart_parsed_str,
                    parent_key="data/com/nasdaq/market/stock/" + stock_name + "/chart/data/chart",
                )
            )

    except requests.exceptions.ConnectionError:
        pass

    logger.info("Updated com/nasdaq/market")

    scheduler.enter(15, 1, update_data, argument=(scheduler,))


def run():
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Running com/nasdaq/market data_source:")

    s = sched.scheduler(time.time, gevent.sleep)

    s.enter(0, 1, update_data, argument=(s,))
    s.run()


if __name__ == "__main__":
    s = sched.scheduler(time.time, gevent.sleep)

    s.enter(0, 1, update_data, argument=(s,))
    s.run()
