import gevent

import logging
import multiprocessing
import sched
import time
import csv

from datetime import datetime

import requests

from streamgraphicserver.util.remote_data import send_data_multiple
from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.other import flatten
from streamgraphicserver.util.restore import run_script

DOCUMENT_EVENTS = "https://docs.google.com/spreadsheets/d/1vNtnA69D5tLbxpfmA84Zr24nxv_lK2UGVk9anbDiXZs/export?gid=1204353778&format=csv"

events = []

curr_pos = 0


def update_data(scheduler):
    global events

    logger = logging.getLogger(__name__)

    try:
        logger.info("Requesting events.")
        r = requests.get(DOCUMENT_EVENTS, headers={"User-Agent": "enzanki-stream"}, timeout=(3.05, 27))
        logger.info("Got events.")

        csvreader_events = csv.DictReader(r.text.split("\n"))
        events = []
        for row in csvreader_events:
            if row["announce"] == "yes":
                date = datetime.fromisoformat(row["date_iso"])
                row["date-text"] = date.strftime("%-m/%-d")
                row["time-text"] = date.strftime("%-I:%M %p")

                events.append(row)

        # send_data_multiple(flatten(info, parent_key='data/io/gitlab/osubgc/results'))
    except requests.exceptions.ConnectionError:
        logger.error("Failed to connect for events.")

    scheduler.enter(60, 1, update_data, argument=(scheduler,))


def update_cycle(scheduler):
    global events

    global curr_pos

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)

    total_show = len(events)

    if total_show > 0:
        # Ensure that curr_pos is within the displayable list.
        curr_pos = curr_pos % total_show
    else:
        pass

    # curr_pos = 0

    logger.info("Displaying %i/%i", curr_pos + 1, total_show)

    do_transition_show("events")
    send_data_multiple(flatten(events[curr_pos], parent_key="data/io/gitlab/osubgc/events/cycle"))
    do_transition_hide("events")

    logger.info("Done displaying %i/%i", curr_pos + 1, total_show)

    # Move to the next result
    curr_pos = (curr_pos + 1) % total_show

    scheduler.enter(12.5, 1, update_cycle, argument=(scheduler,))


def do_transition_show(type):
    run_script("restore/edu/osu/org/osubgc/overlays/lower-third/center/" + type, "ticker_transition_show")
    time.sleep(0.75)


def do_transition_hide(type):
    run_script("restore/edu/osu/org/osubgc/overlays/lower-third/center/" + type, "ticker_transition_hide")
    time.sleep(0.75)


def run():
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Starting io/gitlab/osubgc/results data_source.")

    scheduler = sched.scheduler(time.time, gevent.sleep)
    scheduler.enter(0, 1, update_data, argument=(scheduler,))
    scheduler.enter(0, 1, update_cycle, argument=(scheduler,))
    scheduler.run()


if __name__ == "__main__":
    run()
