import logging
import multiprocessing
import sched
import time

import feedparser
import gevent
import requests
from bs4 import BeautifulSoup
from streamgraphicserver.data_sources.com.open_metro.forecast import update_weather
from streamgraphicserver.data_sources.io.github.enzanki_ars.status import run_get_status
from streamgraphicserver.util.remote_data import send_data_multiple
from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.other import flatten
from streamgraphicserver.util.restore import run_script_no_thread

API_URL = "http://enzanki-mini:8002"
API_DATA_PATH = "/api/v1/set_multiple_for_path_and_flatten/data"
API_PATH_HACKER_NEWS = "data/com/ycombinator/news/best"
API_PATH_CURR_RSS = "data/io/github/enzanki-ars/home/rss/curr"
API_RESTORE_PATH = "restore/io/github/enzanki-ars/overlays/home/home"

REQUESTS_SESSION = requests.Session()

PAUSE_TIME = 10
GLOBAL_LIMIT = 5

RSS_FEEDS = [
    {
        "api_path": "data/uk/co/bbc/news/world",
        "name": "BBC News - World",
        "domain": "bbc.co.uk",
        "url": "http://feeds.bbci.co.uk/news/world/rss.xml",
        "logo_image": "/static/local/news/bbc_news.png",
    },
    {
        "api_path": "data/uk/co/bbc/news/technology",
        "name": "BBC News - Tech",
        "domain": "bbc.co.uk",
        "url": "http://feeds.bbci.co.uk/news/technology/rss.xml",
        "logo_image": "/static/local/news/bbc_news.png",
    },
    {
        "api_path": "data/org/npr/news/national",
        "name": "NPR News - National",
        "domain": "npr.org",
        "url": "https://feeds.npr.org/1003/rss.xml",
        "logo_image": "/static/local/news/npr_news.png",
    },
    {
        "api_path": "data/org/npr/news/technology",
        "name": "NPR News - Technology",
        "domain": "npr.org",
        "url": "https://feeds.npr.org/1019/rss.xml",
        "logo_image": "/static/local/news/npr_news.png",
    },
    {
        "api_path": "data/com/phoronix/news/recent",
        "name": "Phoronix",
        "domain": "phoronix.com",
        "url": "https://www.phoronix.com/rss.php",
        "logo_image": "/static/local/news/phoronix.png",
    },
    {
        "api_path": "data/com/thelantern/feed",
        "name": "The Lantern",
        "domain": "thelantern.com",
        "url": "https://www.thelantern.com/feed/",
        "logo_image": "/static/local/news/the_lantern.png",
    },
    {
        "api_path": "data/com/10tv/feed/local",
        "name": "10tv - Local",
        "domain": "10tv.com",
        "url": "https://www.10tv.com/feeds/syndication/rss/news/local",
        "logo_image": "/static/local/news/wbns.png",
    },
]


def wait_countdown(path="data/io/github/enzanki-ars/home/rss/curr", length=PAUSE_TIME):
    for i in range(length):
        send_data_multiple(
            flatten(
                {
                    "countdown": length - i,
                },
                parent_key=path,
            )
        )

        time.sleep(1)


def run_home_lower_third(scheduler, lat, long):
    logger = logging.getLogger(__name__)

    for feed in RSS_FEEDS:
        run_rss(logger, feed)
        run_weather(lat, long, logger)
        # run_coinbase(logger)

        run_script_no_thread(API_RESTORE_PATH, "status")
        wait_countdown(length=15)

    run_hn(logger)
    run_weather(lat, long, logger)
    # run_coinbase(logger)

    run_script_no_thread(API_RESTORE_PATH, "status")
    wait_countdown(length=15)

    scheduler.enter(0, 0, run_home_lower_third, argument=(scheduler, lat, long))


def run_weather(lat, long, logger):
    logger.info("Starting Weather Info")
    update_weather(lat, long, location_name="local")

    run_script_no_thread(API_RESTORE_PATH, "weather")
    wait_countdown(length=20)


def run_coinbase(logger):
    logger.info("Displaying Coinbase info.")

    run_script_no_thread(API_RESTORE_PATH, "coinbase")
    wait_countdown(length=20)


def run_hn(logger):
    hn_best_stories = REQUESTS_SESSION.get("https://hacker-news.firebaseio.com/v0/topstories.json").json()[0:GLOBAL_LIMIT]

    logger.info("Starting HN Story Loop")
    logger.info("HN Stories: " + str(len(hn_best_stories)))
    run_script_no_thread(API_RESTORE_PATH, "hn")
    for rank, hn_story in enumerate(hn_best_stories):
        rank += 1

        logger.info(" - HN Story [" + str(rank) + "/" + str(len(hn_best_stories)) + "]: " + str(hn_story))
        hn_story_info = REQUESTS_SESSION.get("https://hacker-news.firebaseio.com/v0/item/" + str(hn_story) + ".json").json()

        description = ""

        if "url" in hn_story_info:
            response = requests.get(hn_story_info["url"])
            soup = BeautifulSoup(response.text, features="lxml")

            for meta_type in [{"name": "description"}, {"property": "og:description"}, {"property": "twitter:description"}]:
                if not description:
                    meta = soup.find("meta", attrs=meta_type, content=True)

                    if meta:
                        description = meta["content"]

        send_data_multiple(
            flatten(
                {
                    "name": "Hacker News",
                    "rank": rank,
                    "stories": len(hn_best_stories),
                    "title": hn_story_info.get("title", ""),
                    "score": hn_story_info.get("score", ""),
                    "comments": hn_story_info.get("descendants", ""),
                    "logo": "/static/local/news/hacker_news.png",
                    "description": description,
                },
                parent_key=API_PATH_HACKER_NEWS,
            )
        )

        wait_countdown(path="data/com/ycombinator/news/best")


def run_rss(logger, feed):
    logger.info("Parsing RSS Feed for " + feed["name"])
    run_script_no_thread(API_RESTORE_PATH, "rss")
    parsed_feed = feedparser.parse(feed["url"])

    for rank, entry in enumerate(parsed_feed.entries[0:GLOBAL_LIMIT]):
        rank += 1

        logger.info(" - " + feed["name"] + " Story [" + str(rank) + "/" + str(len(parsed_feed.entries[0:GLOBAL_LIMIT])) + "]")

        sg_data = {
            "rank": rank,
            "stories": len(parsed_feed.entries[0:GLOBAL_LIMIT]),
            "title": entry["title"],
            "description": "" if feed.get("ignore-description", False) else entry["description"],
            "source": entry.get("source"),
            "name": feed["name"],
            "logo": feed["logo_image"],
        }
        send_data_multiple(flatten(sg_data, parent_key=feed["api_path"]))
        send_data_multiple(flatten(sg_data, parent_key=API_PATH_CURR_RSS))
        wait_countdown()


def run(lat, long):
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Starting io/github/enzanki_ars data_source.")

    scheduler = sched.scheduler(time.time, gevent.sleep)
    scheduler.enter(0, 1, run_home_lower_third, argument=(scheduler, str(lat), str(long)))
    scheduler.run()


if __name__ == "__main__":
    print("This script is not designed to be run independently.")
