import logging
import multiprocessing
import sched
import time
import gevent

import requests

from streamgraphicserver.util.logging import setup_loggers
from streamgraphicserver.util.remote_data import send_data_multiple
from streamgraphicserver.util.other import flatten

team_info_etag = ""


def update_data(scheduler=None, team=52335):
    global team_info_etag

    logger = logging.getLogger(__name__)

    session = requests.Session()
    session.trust_env = False

    dd_api_team = session.get(
        "https://www.extra-life.org/api/teams/" + team,
        timeout=(3.05, 27),
        headers={"If-None-Match": team_info_etag},
    )

    if dd_api_team.status_code == 200:
        logger.info("data/org/extra-life/donations/team: NEW DONATION(s).")

        team_info = dd_api_team.json()
        team_info_etag = dd_api_team.headers.get("etag")

        original_goal = 5000

        team_info["currentGoalProgress"] = team_info["sumDonations"] / team_info["fundraisingGoal"]
        team_info["originalGoalProgress"] = team_info["sumDonations"] / original_goal

        send_data_multiple(flatten(team_info, parent_key="data/org/extra-life/donations/team/" + team))

        dd_api_team_donations = session.get(
            "https://www.extra-life.org/api/teams/" + team + "/donations?orderBy=createdDateUTC+DESC",
            timeout=(3.05, 27),
        )

        if dd_api_team_donations.status_code == 200:
            team_info_donations = dd_api_team_donations.json()

            for donation in team_info_donations:
                if "displayName" not in donation:
                    donation["displayName"] = "Anonymous"

            send_data_multiple(
                flatten(
                    team_info_donations,
                    parent_key="data/org/extra-life/donations/team/" + team + "/donations",
                )
            )

        dd_api_team_topdonor = session.get(
            "https://www.extra-life.org/api/teams/" + team + "/donors?orderBy=sumDonations+DESC&limit=1",
            timeout=(3.05, 27),
        )

        if dd_api_team_topdonor.status_code == 200:
            team_info_topdonor = dd_api_team_topdonor.json()

            send_data_multiple(
                flatten(
                    team_info_topdonor,
                    parent_key="data/org/extra-life/donations/team/" + team + "/topdonor",
                )
            )
    elif dd_api_team.status_code == 304:
        logger.info("data/org/extra-life/donations/team: no change.")

    scheduler.enter(15, 1, update_data, argument=(scheduler, team))


def run(team=52335):
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Starting org/extra-life/donations data_source: team=%s", team)
    scheduler = sched.scheduler(time.time, gevent.sleep)

    scheduler.enter(0, 1, update_data, argument=(scheduler, team))
    scheduler.run()
