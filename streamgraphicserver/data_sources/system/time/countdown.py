import datetime
import logging
import multiprocessing
import sched
import time

from dateutil.parser import parse
import gevent

from streamgraphicserver.util.remote_data import send_data
from streamgraphicserver.util.logging import setup_loggers


def get_time(
    scheduler,
    countdown=str(datetime.datetime.now().replace(second=0, microsecond=0).isoformat()),
):
    logger = logging.getLogger(__name__)

    delta = parse(countdown) - datetime.datetime.now()

    str_delta = str(abs(delta - datetime.timedelta(microseconds=delta.microseconds)))

    if str_delta.startswith("0:"):
        str_delta = str_delta[len("0:") :]

    send_data("data/system/countdown", str_delta)

    # logger.info(str_delta)

    scheduler.enter(1, 1, get_time, argument=(scheduler, countdown))


def run(countdown=str(datetime.datetime.now().replace(second=0, microsecond=0).isoformat())):
    setup_loggers()

    multiprocessing_logger = multiprocessing.get_logger()
    multiprocessing_logger.setLevel(logging.INFO)

    logger = logging.getLogger(__name__)

    logger.info("Starting system/time/countdown data_source.")

    logger.info("Counting down to %s", parse(countdown))

    scheduler = sched.scheduler(time.time, gevent.sleep)
    scheduler.enter(0, 1, get_time, argument=(scheduler, countdown))
    scheduler.run()


if __name__ == "__main__":
    run()
