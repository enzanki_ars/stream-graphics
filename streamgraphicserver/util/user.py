import json
import random
import re
import string
import traceback

import bcrypt
import flask_login
from streamgraphicserver.util.config import get_config

CONFIG_USER_API_REGEX = re.compile(r"^user/(?P<username>.*)/api_key$")


class User(flask_login.UserMixin):
    username = None
    password = None
    api_key = None
    authenticated = False

    def __init__(self, username):
        self.username = username
        self.password = get_config().get("user/" + username, None)
        self.api_key = get_config().get("user/" + username + "/api_key", None)

    def is_active(self):
        """True, as all users are active."""
        return True

    def get_id(self):
        """Return the email address to satisfy Flask-Login's requirements."""
        return self.username

    def is_authenticated(self):
        """Return True if the user is authenticated."""
        return self.authenticated

    def is_anonymous(self):
        """False, as anonymous users aren't supported."""
        return False

    def create_reset_api_key(self):
        config = get_config()

        self.api_key = "".join(random.sample(string.ascii_lowercase + string.ascii_uppercase + string.digits, 32))

        config["user/" + self.username + "/api_key"] = self.api_key

        with open("config.json", "w") as json_data_new:
            json.dump(config, json_data_new, indent=4)

        return self.api_key

    def find_api(api_key):
        config = get_config()

        for key, value in config.items():
            if CONFIG_USER_API_REGEX.search(key):
                username = CONFIG_USER_API_REGEX.search(key).group("username")
                try:
                    if api_key == value:
                        return User(username)
                except:
                    traceback.print_exc()
        return None
