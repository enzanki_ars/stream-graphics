import requests
from streamgraphicserver.util.config import get_config
from urllib3.exceptions import InsecureRequestWarning

# Suppress only the single warning from urllib3 needed.
requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)

API_URL = "http://localhost:8002"
API_CLEAR_PATH = "/api/v1/clear/"
API_DATA_PATH = "/api/v1/set_multiple"
API_EVENT_PATH = "/api/v1/alert_overlay/"
REQUESTS_SESSION = requests.Session()

SG_API_KEY = get_config()["user/system/api_key"]
REQUESTS_SESSION.params["api_key"] = SG_API_KEY
REQUESTS_SESSION.verify = False


def delete_keys(search=None):
    REQUESTS_SESSION.delete(API_URL + API_CLEAR_PATH + search)


def send_data(path, data):
    REQUESTS_SESSION.post(API_URL + API_DATA_PATH, json={path: data})


def send_data_multiple(all_data):
    REQUESTS_SESSION.post(API_URL + API_DATA_PATH, json=all_data)


def send_alert_overlay(
    log_name,
    text_right="",
    text_left="",
    fa_icon="",
    image="",
    separator=": ",
    bg_color="black",
    text_color="white",
    time=5000,
):
    log_settings = {
        "text_right": text_right,
        "text_left": text_left,
        "image": image,
        "fa_icon": fa_icon,
        "separator": separator,
        "bg_color": bg_color,
        "text_color": text_color,
        "time": time,
    }
    REQUESTS_SESSION.post(
        API_URL + API_EVENT_PATH + log_name,
        params=log_settings,
    )
