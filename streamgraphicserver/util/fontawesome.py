import subprocess
import sys

from flask.blueprints import Blueprint
from streamgraphicserver.util.config import get_config

FONT_AWESOME_VERSION = get_config().get("config/system/fontawesome/version", "5.15.4")


def install_fontawesome(__name__, app, logger):
    fontawesome_static_path = "invalid"

    if get_config().get("config/system/fontawesome/pro_token", None):
        logger.info("Loading Font Awesome Pro.")
        subprocess.check_call(
            [
                "python",
                "-m",
                "pip",
                "install",
                "--extra-index-url",
                "https://dl.fontawesome.com/" + get_config().get("config/system/fontawesome/pro_token") + "/fontawesome-pro/python/simple/",
                "fontawesomepro==" + FONT_AWESOME_VERSION,
                "--quiet",
            ]
        )

        try:
            import fontawesomepro

            fontawesome_static_path = fontawesomepro.__path__[0] + "/static/fontawesomepro"

            fontawesomepro_blueprint = Blueprint(
                "fontawesome",
                __name__,
                static_folder=fontawesome_static_path,
                url_prefix="/assets/fontawesomepro",
            )
            app.register_blueprint(fontawesomepro_blueprint)
            logger.info("Font Awesome Pro was loaded via path: %s", fontawesome_static_path)
        except:
            logger.warn("Font Awesome Pro could not be loaded.")
    else:
        logger.warn("Font Awesome Pro unavailable.  Attempting to load Font Awesome Free.")
        subprocess.check_call(
            [
                "python",
                "-m",
                "pip",
                "install",
                "fontawesomefree==" + FONT_AWESOME_VERSION,
                "--quiet",
            ]
        )
        try:
            import fontawesomefree

            fontawesome_static_path = fontawesomefree.__path__[0] + "/static/fontawesomefree"

            fontawesomefree_blueprint = Blueprint(
                "fontawesome",
                __name__,
                static_folder=fontawesome_static_path,
                url_prefix="/assets/fontawesomefree",
            )
            app.register_blueprint(fontawesomefree_blueprint)
            logger.info("Font Awesome Free was loaded via path: %s", fontawesome_static_path)

        except:
            logger.warn("Font Awesome Free could not be loaded.  Not Loading Font Awesome Pro either.")

    return fontawesome_static_path
