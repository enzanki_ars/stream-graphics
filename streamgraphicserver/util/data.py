import base64
import fnmatch
import gzip
import json
import os

import redis
from flask_socketio import SocketIO

from streamgraphicserver.util.other import flatten

cache = {}
KEYFRAME_TIME = 5
last_keyframe = None

ENABLE_COMPRESSION = True
ENABLE_DATA_LOGGING = False


def get_item(key):
    return REDIS_CONNECTION.hget("all_data", key)


def get_item_jinja(key):
    value = REDIS_CONNECTION.hget("all_data", key)

    return value if value else ""


def get_mix_overlays(key):
    return REDIS_CONNECTION.hget("all_data", "mix/" + key).split(",")


def get_data(search=None, use_cache=True):
    if cache and use_cache:
        if search:
            data = {match_key: cache[match_key] for match_key in fnmatch.filter(cache.keys(), search)}
        else:
            data = cache
    else:
        data = REDIS_CONNECTION.hscan("all_data", match=search, count=1000000000)[1]

    return data


def get_keys(search=None):
    data = []

    for key, value in REDIS_CONNECTION.hscan_iter("all_data", match=search, count=1000000000):
        data.append(key)

    return data


def delete_key(key):
    REDIS_CONNECTION.hdel("all_data", key)


def delete_keys(search=None):
    keys_to_del = get_keys(search=search)

    if len(keys_to_del) > 0:
        REDIS_CONNECTION.hdel("all_data", *keys_to_del)


def compress_json(data):
    json_str = json.dumps(data)
    gzip_compress = gzip.compress(json_str.encode("utf-8"))
    b64_str = base64.b64encode(gzip_compress)

    return b64_str.decode("utf-8")


def update_data(path, data):
    global cache

    if data is None:
        data = ""

    if path not in cache.keys() or cache[path] != data:
        cache[path] = data

        if ENABLE_COMPRESSION:
            socket_io.emit("update_compressed", compress_json({"path": path, "data": str(data)}), namespace="/websocket")
        else:
            socket_io.emit("update", {"path": path, "data": str(data)}, namespace="/websocket")

        REDIS_CONNECTION.hset("all_data", path, str(data))
        if ENABLE_DATA_LOGGING:
            REDIS_CONNECTION.xadd("data_log", {path: str(data)})


def update_data_multiple(all_data):
    global cache

    uncached = {}

    for key, value in all_data.items():
        if value is None:
            value = ""

        if key not in cache.keys() or cache[key] != value:
            uncached[key] = value

    cache = {**cache, **uncached}

    if uncached:
        if ENABLE_COMPRESSION:
            socket_io.emit("update_multiple_compressed", compress_json(uncached), namespace="/websocket")
        else:
            socket_io.emit("update_multiple", uncached, namespace="/websocket")

        REDIS_CONNECTION.hset("all_data", mapping=uncached)
        if ENABLE_DATA_LOGGING:
            REDIS_CONNECTION.xadd("data_log", {path: str(data) for path, data in uncached.items()})


def get_log(log="data_log", start="-", end="+"):
    return REDIS_CONNECTION.xrange(log, min=start, max=end)


def send_alert_overlay(
    alert_channel="",
    text_right="",
    text_left="",
    fa_icon="",
    image="",
    separator=": ",
    bg_color="black",
    text_color="white",
    time=5000,
):
    log_settings = {
        "alert_channel": alert_channel,
        "text_right": text_right,
        "text_left": text_left,
        "image": image,
        "fa_icon": fa_icon,
        "separator": separator,
        "bg_color": bg_color,
        "text_color": text_color,
        "time": time,
    }

    REDIS_CONNECTION.xadd("alert_overlay", log_settings)
    socket_io.emit("alert_overlay", log_settings, namespace="/websocket")


def send_data(path, data):
    update_data(path, data)


def send_data_multiple(all_data):
    processed = dict([key, str(value) if value != None else ""] for key, value in all_data.items())

    update_data_multiple(processed)


def send_data_multiple_and_flatten(all_data, parent_key):
    send_data_multiple(flatten(all_data, parent_key=parent_key))


REDIS_IP = os.getenv("REDIS_IP", "localhost")
REDIS_PORT = os.getenv("REDIS_PORT", "6379")
REDIS_DB = os.getenv("REDIS_DB", "0")
REDIS_HOST = "redis://" + REDIS_IP + ":" + str(REDIS_PORT)
REDIS_CONNECTION = redis.StrictRedis(host=REDIS_IP, port=REDIS_PORT, db=REDIS_DB, decode_responses=True)
socket_io = SocketIO(async_mode="gevent", message_queue=REDIS_HOST)

print("Building cache.")
cache = REDIS_CONNECTION.hscan("all_data", count=1000000000)[1]
print("Built cache.")
