import configparser
import glob
import logging
import os

from jinja2 import Environment, select_autoescape
from jinja2.loaders import ChoiceLoader, FileSystemLoader
from ruamel.yaml import YAML
from semantic_version import SimpleSpec, Version

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

yaml = YAML(typ="safe")

JINJA_ENV = Environment(
    loader=ChoiceLoader([FileSystemLoader(".")]),
    autoescape=select_autoescape([]),
)


def get_editor_config(template_path, template_name, variables={}):
    short_name = os.path.join("restore", template_path, template_name)

    restore_template = JINJA_ENV.get_template(short_name + ".yaml.j2")

    if os.path.exists(short_name + ".cfg"):
        config = configparser.ConfigParser()
        config.read_file(open(short_name + ".cfg"))

        jinja_vars = config["DEFAULT"]
    else:
        jinja_vars = {}

    jinja_vars.update(variables)

    return yaml.load(restore_template.render(**jinja_vars))


def list_editor_files():
    template_restore = [y for y in glob.iglob("restore/**/editors/**/*.yaml.j2", recursive=True)]

    return template_restore


def list_editor_file_names():
    template_restore = [y.replace(".yaml.j2", "") for y in glob.iglob("restore/**/editors/**/*.yaml.j2", recursive=True)]

    return template_restore


def list_editor_properties():
    import time

    editor_list = []

    for restore_filename in list_editor_files():
        tic = time.perf_counter()

        short_name = "/".join(restore_filename.split(os.path.sep)[1:])[: -len(".yaml.j2")]

        folder = (os.path.sep).join(short_name.split(os.path.sep)[:-1])
        name = short_name.split(os.path.sep)[-1]

        data = get_editor_config(folder, name)

        toc = time.perf_counter()

        print("Read", short_name, f"in {toc - tic:0.4f} seconds")

        if "header" in data:
            editor_list.append(
                {
                    "link": short_name,
                    "name": "",
                    "file": restore_filename,
                    "supported": False,
                }
            )

            if "version" in data["header"]:
                if Version(data["header"]["version"]) in SimpleSpec(">=12.0.0,<13.0.0"):
                    editor_list[-1]["supported"] = True

        if "page" in data:
            if "title" in data["page"]:
                editor_list[-1]["name"] = data["page"]["title"]

    return sorted(editor_list, key=lambda i: i["name"])
