# enzanki_ars's Stream Graphics

Current Version: _v14.0.0_

## Requirements

- Python 3+
- Redis

A Docker file will be provided soon to make set up easier.  

## Compatible Browsers

- Latest Google Chrome
- Latest OBS Browser Source
  - Has not been tested in OBS forks such as Streamlabs.

## Known issues

- Skew option should not be used when a component spans more than one row.
  - This can cause undesired overlapping of components to occur.

## Upgrading

### Upgrading from v11.0.0

v12.0.0 is a major overhaul of the overlay style structure. Instead of defining
a location for all desired aspect ratios, only one aspect ratio style can now
be specified. This will allow for more responsive design of overlays and lay
the foundation for future work in utilizing the overlay system embedded on the
web with the ability for users to edit their view of the broadcast as desired.
As a result of the major change, it is not possible to provide a
find-and-replace style system for migration. Overlays will need to be
transferred manually. Additionally, as this version now matches the longer term
vision for the software, documentation is being worked on and will make the
creation and understanding of the overlay files much easier.

### Upgrading from v10.0.0

Starting in v11.0.0, the restoration folder structure has been updated. To
update to the new structure, use the following information. Notably, the
folders are now under `restore/`, and overlays and editors are stored in
their `domain_path`, such as `com/example/`, under subfolders called
`overlays/` and `editors/`.

- `restore/domain_path/overlay_name.yaml.j2` --> `restore/domain_path/overlays/overlay_name.yaml.j2`
- `editors/domain_path/editor_name.yaml.j2` --> `restore/domain_path/editors/editor_name.yaml.j2`

### Upgrading from v9.0.0

Starting in v10.0.0, overlays were renamed to layers and premixes were renamed
to overlays. The intent is to improve readability and understanding of how the
replay server works and the file format.

As a result, **_all_** overlays must be reinitialized and converted to the new
v10.0.0 format. It is best to delete the previous database and start over
from scratch. To convert old overlays, a find-and-replace in this order
will ensure that all overlays meet the new version.

- `overlay` --> `layer`
- `premix` --> `overlay`

## Future plans

- ~~Add graphical overlay designer/editor.~~
  - Given the recent addition of the template engine, it has been decided
  that this project works better as a tool for programers without graphical
  design backgrounds to create overlays. As a result, no graphical
  overlay designer/editor will be included in it's current state. This may
  change in the future given time and availability.

## Demo

This screenshot is from a much older version, but the design, layout, and
features are similar.

![alt text](https://gitlab.com/enzanki_ars/stream-graphics/raw/master/demo.png)
