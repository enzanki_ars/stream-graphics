FROM python:slim
WORKDIR /app
ADD requirements.txt /app
RUN pip install -r requirements.txt
ADD . /app
EXPOSE 8000
# gunicorn -k geventwebsocket.gunicorn.workers.GeventWebSocketWorker -w 1 streamgraphicserver.server:app -b 0.0.0.0:8000
CMD ["gunicorn", "-k", "geventwebsocket.gunicorn.workers.GeventWebSocketWorker", "-w", "1", "streamgraphicserver.server:app", "-b", "0.0.0.0:8000"]
